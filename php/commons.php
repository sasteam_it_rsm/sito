<?php
  /* Disegna il footer del sito
     E' necessario includere il file "footer.css" all'interno
     della pagina dove viene richiamata questa funzione.
  */


  function draw_footer() {
    echo '
      <footer class="myfooter">
    		<div class="container">
    			<div class="row">
    				<div class="col-sm-6">
    					<h4>
                <i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i>
                Trovaci sui social
              </h4>
    					<p>Qui sotto potrai trovare tutti i links sulle varie piattaforme!</p>

    					<ul class="social social-circle">
    						<li> <a aria-label="Facebook" href="https://it-it.facebook.com/" class="icoFacebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    						<li> <a aria-label="Twitter" href="https://twitter.com/" class="icoTwitter"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
    						<li> <a aria-label="Google+" href="https://plus.google.com/discover?hl=it" class="icoGoogle"> <i class="fa fa-google-plus" aria-hidden="true"></i> </a> </li>
    						<li> <a aria-label="Instagrm" href="https://www.instagram.com/" class="icoRss"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
    					</ul>
    				</div>
    				<div class="col-sm-6">
    					<h4>
                <i class="fa fa-eur fa-lg" aria-hidden="true"></i>
                Metodi di pagamento
              </h4>
    					<p>Accettiamo diversi metodi di pagamento, nessuna commissione aggiuntiva!</p>
    					<ul class="payment">
    						<li><a aria-label="American Express" href="https://www.americanexpress.com/it/"><i class="fa fa-cc-amex zoom" aria-hidden="true"></i></a></li>
    						<li><a aria-label="Mastercard" href="https://www.mastercard.it/it-it.html"><i class="fa fa-cc-mastercard zoom" aria-hidden="true"></i></a></li>
    						<li><a aria-label="Visa" href="https://www.visaitalia.com/"><i class="fa fa-cc-visa zoom" aria-hidden="true"></i></a></li>
                <li><a aria-label="Bitcoin" href="https://bitcoin.org/it/"><i class="fa fa-btc zoom" aria-hidden="true"></i></a></li>
    					</ul>
    				</div>
    			</div>
    			<hr>
    			<p class="text-center">Copyright © Sgulvanid 2017 - Tutti i diritti riservati</p>
      	</div>
      </footer>';
  }

  function draw_profilo($depth){
    echo '<div class="container-fluid">
      <div class="row">
        <div class="page-header col-md-12">
          <h1> <i class="fa fa-bars" aria-hidden="true"></i> Profilo</h1>
          <ol class="breadcrumb">
            <li><a href="'; location_resource($depth); echo 'index.php">Home</a></li>
            <li class="active">Profilo</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="container-fullwidth visible-xs-block visible-sm-block visible-md-block visible-lg-block">
      <div class="navbar navbar-inverse profilo">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <ul  type="none">
                <li>
                    <a class="text" href="#">Il mio profilo</a>
                </li>
                <li>
                  <a class="fancy-btn text" href="'; location_resource($depth); echo 'dati_personali.php">I miei dati personali</a>
                </li>
                <li>
                    <a class="fancy-btn text" href="'; location_resource($depth); echo 'indirizzo.php">Indirizzo di spedizione</a>
                </li>
                <li>
                  <a class="fancy-btn text" href="'; location_resource($depth); echo 'ordini.php">Storico degli ordini</a>
                </li>';

                if($_SESSION["isAdmin"]) {
                  echo '
                  <li>
                    <a class="fancy-btn text" href="'; location_resource($depth); echo 'profilo_gestione_ordini.php">Gestione ordini (ADMIN)</a>
                  </li>';
                }

                echo '
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>';

  }

  function dram_tmp($percorsoImmagine){
   echo '<div class="col-sm-2 hidden-xs"><img src="$percorsoImmagine " alt="..." class="img-responsive"/></div>';
  }

  /* Disegna il menu (responsive) */
  function draw_menu($mysqli,$depth) {
  echo '
  <div class="iconatmp_position container-fullwidth visible-sm-block">
    <div class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
          <a href="'; location_resource($depth); echo 'index.php">
              <span class="sr-only">Vai alla index</span>
              <img class="img-responsive center-block iconatmp_size" src="'; location_resource($depth); echo 'img/logo.png" alt="">
          </a>
          </div>
          <div class="col-sm-6">
            <ul  type="none">
              <li>
                  <a class="fancy-btn" href="'; location_resource($depth); echo 'shopping_cart.php">
                    <span class="sr-only">Vai al carrello</span>
                     <i class="fa fa-shopping-cart" aria-hidden="true"> </i>
                  </a> ';

                  if(!login_check($mysqli)){
                  echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'php/login.php">
                    <span class="sr-only">Login</span>
                       <i class="fa fa-user-circle-o" aria-hidden="true"> Accedi</i>
                    </a>';
                  }else{
                    echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'profilo.php">
                      <span class="sr-only">Vai al profilo</span>
                        <i class="fa fa-user-circle-o" aria-hidden="true"> Profilo</i>
                      </a>';
                  }

        echo '</li>
              <li class="prova">
              <a class="fancy-btn" data-container="body" href="'; location_resource($depth); echo 'notifications.php">
                <span class="sr-only">Vai alle notifiche</span>
                <i class="fa fa-bell-o" aria-hidden="true"></i>
              </a>';

                  if (!login_check($mysqli)) {
                    echo '<a class="fancy-btn plus" href="'; location_resource($depth); echo 'php/register.php">
                        <span class="sr-only">Registrati</span>
                      <i class="fa fa-user-plus" aria-hidden="true"> Registrati</i>
                    </a>';
                  }
                  else{
                    echo '<a class="fancy-btn plus" href="'; location_resource($depth); echo 'php/logout.php" style="padding: 10px 70px;">
                        <span class="sr-only">Effettua il logout</span>
                      <i class="fa fa-sign-out" aria-hidden="true"> Logout</i>
                    </a>';
                  }

                  echo '
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fullwidth">
      <!-- Second navbar for search -->
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed pull-left hamburger" data-toggle="collapse" data-target="#navbar-collapse-3">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="visible-xs-block">
              <a class="fancy-btn cart pull-right" href="'; location_resource($depth); echo 'shopping_cart.php" style="margin-top: 0px">
                  <span class="sr-only">Vai al carrello</span>
                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
              </a>
              <a class="fancy-btn bell pull-right" data-container="body" href="'; location_resource($depth); echo 'notifications.php" style="margin-top: 0px">
              <span class="sr-only">Vai alle notifiche</span>
                <i class="fa fa-bell-o" aria-hidden="true"></i>
              </a>
              <a href="'; location_resource($depth); echo 'index.php">
                  <img class="img-responsive center-block iconatmp_size" src="'; location_resource($depth); echo 'img/logo.png" alt="Logo" style="margin-top: 15px; overflow: hidden;">
                  <span class="sr-only">Vai alla home</span>
              </a>

            </div>
          </div>
          <div class="navbar visible-md-block visible-lg-block">
            <div class="row">
              <div class="col-md-4">
              <a href="'; location_resource($depth); echo 'index.php">
                  <img class="img-responsive center-block iconatmp_size" src="'; location_resource($depth); echo 'img/logo.png" alt="';  echo 'errore">
                  <span class="sr-only">Vai alla home</span>
              </a>
              </div>
              <div class="col-md-4 center-block text-center">
                <form class="navbar-form position" role="search" method="POST" action="' . location_resource($depth) . 'search.php">
                  <div class="form-group">
                    <label for="ricerca" class="sr-only">Ricerca</label>
                    <input type="text" class="form-control position" id="ricerca" name="ricerca" placeholder="Cerca" />
                  </div>
                </form>
              </div>
              <div class="col-md-4 position" >
              ';
              if(!login_check($mysqli)){
              echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'php/login.php" >
                  <span class="sr-only">Vai al login</span>
                 <i class="fa fa-user-circle-o" aria-hidden="true"> Accedi</i>
              </a>';
              }else{
                echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'profilo.php">
                <span class="sr-only">Vai al profilo</span>
                   <i class="fa fa-user-circle-o" aria-hidden="true"> Profilo</i>
                </a>';
              }

              if (!login_check($mysqli)) {
                echo '<a class="fancy-btn plus" href="'; location_resource($depth); echo 'php/register.php">
                <span class="sr-only">Registrati</span>
                  <i class="fa fa-user-plus" aria-hidden="true"> Registrati</i>
                </a>';
              }
              else{
                echo '<a class="fancy-btn plus" href="'; location_resource($depth); echo 'php/logout.php">
                <span class="sr-only">Effettua il logout</span>
                  <i class="fa fa-sign-out" aria-hidden="true"> Logout</i>
                </a>';
              }
              echo '
                <a class="fancy-btn cart position" href="'; location_resource($depth); echo 'shopping_cart.php">
                <span class="sr-only">Vai al carrello</span>
                  <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                </a>
                <a class="fancy-btn position bell" data-container="body" href="'; location_resource($depth); echo 'notifications.php">
                <span class="sr-only">Vai alle notifiche</span>
                  <i class="fa fa-bell-o" aria-hidden="true"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="navbar-collapse-3">
              <ul class="nav navbar-nav navbar-right visible-xs-block">
                <li>
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-6">
                      ';
                          if(!login_check($mysqli)){
                          echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'php/login.php">
                          <span class="sr-only">Vai al login</span>
                              <i class="fa fa-user-circle-o text-center " aria-hidden="true"><br/>Accedi</i>

                            </a>';
                          }else{
                            echo '<a class="fancy-btn user" href="'; location_resource($depth); echo 'profilo.php">
                            <span class="sr-only">Vai al profilo</span>
                                <i class="fa fa-user-circle-o text-center" aria-hidden="true"><br/>Profilo</i>
                              </a>';
                          }
                          echo '
                      </div>
                      <div class="col-xs-6">';
                          if (!login_check($mysqli)) {
                            echo '<a class="fancy-btn out pull-right" href="'; location_resource($depth); echo 'php/register.php">
                            <span class="sr-only">Registrati</span>
                              <i class="fa fa-user-plus text-center " aria-hidden="true"><br/>Registrati</i>
                            </a>';
                          }
                          else{
                            echo '<a class="fancy-btn out pull-right" href="'; location_resource($depth); echo 'php/logout.php">
                            <span class="sr-only">Effettua il logout</span>
                              <i class="fa fa-sign-out text-center " aria-hidden="true"><br/>Logout</i>

                            </a>';
                          }
                          echo '
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                    <a href="'; location_resource($depth); echo 'index.php#chi_siamo" class="fancy-btn text-center " style="margin-left:20px; margin-right:20px;">Chi siamo</a>
                </li>
                <li>
                    <a href="'; location_resource($depth); echo 'index.php#novita" class="fancy-btn text-center" style="margin-left:20px; margin-right:20px;">Novità</a>
                </li>
                <li>
                    <a href="'; location_resource($depth); echo 'index.php#menu" class="fancy-btn text-center" style="margin-left:20px; margin-right:20px;">Menu</a>
                </li>
                <li>
                    <a href="'; location_resource($depth); echo 'index.php#cosa_dicono" class="fancy-btn text-center cosaDicono" style="margin-left:20px; margin-right:20px;">Cosa dicono di noi</a>
                </li>
                <li>
                  <a class="fancy-btn visible-xs-block text-center" data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3" style="color: black; margin-left: 5px; margin-right: 5px;">Cerca</a>
                </li>
                </ul>
                <ul class="nav navbar-nav navbar-left visible-sm-block" >
                  <li><a href="'; location_resource($depth); echo 'index.php#chi_siamo">Chi siamo</a></li>
                  <li><a href="'; location_resource($depth); echo 'index.php#novita">Novità</a></li>
                  <li><a href="'; location_resource($depth); echo 'index.php#menu">Menu</a></li>
                  <li><a href="'; location_resource($depth); echo 'index.php#cosa_dicono">Cosa dicono di noi</a></li>
                  <li><a class="fancy-btn btn btn-default btn-outline  visible-sm-block text-center"  data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">Cerca</a></li>
                </ul>

            <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse3">
              <div class="visible-xs-block">
                <form class="navbar-form navbar-right" role="search" method="POST" action="' . location_resource($depth) . 'search.php">
                  <div class="form-group">
                    <label for="ricerca" class="sr-only">Ricerca</label>
                    <input type="text" class="form-control" id="ricerca" placeholder="Cerca" name="ricerca">
                  </div>
                  <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Bottone ricerca</span></button>
                  <br>
                    <a class="fancy-btn visible-xs-block text-center col-xs-1" data-toggle="collapse" href="#nav-collapse3" aria-expanded="false" aria-controls="nav-collapse3">
                      <i class="fa fa-arrow-circle-left arrow" aria-hidden="true"></i>
                    </a>
                </form>
              </div>
              <div class="visible-sm-block">
                <form class="navbar-form navbar-left" role="search" method="POST" action="' . location_resource($depth) . 'search.php">
                  <div class="form-group">
                    <label for="ricerca" class="sr-only">Ricerca</label>
                    <input type="text" class="form-control" id="ricerca" placeholder="Cerca" name="ricerca">
                  </div>
                  <button type="submit" class="btn btn-danger"><span class="glyphicon glyphicon-search" aria-hidden="true"></span><span class="sr-only">Premi per cercare</span></button>
                </form>
              </div>
            </div>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
        <div class="container-fullwidth position_menu visible-md-block visible-lg-block">
          <div class="navbar navbar-inverse">
            <ul class="nav navbar-nav visible-md-block visible-lg-block">
              <li><a id="chiSiamo" href="'; location_resource($depth); echo 'index.php#chi_siamo">Chi siamo</a></li>
              <li><a href="'; location_resource($depth); echo 'index.php#novita">Novità</a></li>
              <li><a href="'; location_resource($depth); echo 'index.php#menu">Menu</a></li>
              <li><a id="cosaDicono" href="'; location_resource($depth); echo 'index.php#cosa_dicono">Cosa dicono di noi</a></li>
            </ul>
          </div>
        </div>
      </nav><!-- /.navbar -->
  </div><!-- /.container-fluid -->
  </nav>



  ';

  }


 ?>
