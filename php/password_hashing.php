<?php
require "db_connect.php";
require "functions.php";
if (isset($_POST['nome'], $_POST['cognome'], $_POST['email'], $_POST['password'], $_POST['password_confirm'], $_POST['p'])) {
    // Recupero la password criptata dal form di inserimento.
    $password = $_POST['p'];
    // Crea una chiave casuale
    $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    // Crea una password usando la chiave appena creata.
    $password = hash('sha512', $password.$random_salt);
    // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
    // Assicurati di usare statement SQL 'prepared'.
    $nome = $_POST['nome'];
    $cognome = $_POST['cognome'];
    $email = $_POST['email'];
    $telefono = $_POST['telefono'];
    $isAdmin = '0';
    if ($stmt = $mysqli->prepare("SELECT email FROM UTENTE WHERE email = ? LIMIT 1")) {
       $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
       $stmt->execute(); // esegue la query appena creata.
       $stmt->store_result();
       $stmt->fetch();
       if($stmt->num_rows == 1) {
         header('Location: ./register.php?error=1');
       }
       else if ($insert_stmt = $mysqli->prepare("INSERT INTO UTENTE (nome, cognome, email, password, telefono, salt, isAdmin) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
        $insert_stmt->bind_param('sssssss', $nome, $cognome, $email, $password, $telefono, $random_salt, $isAdmin);
        // Esegui la query ottenuta.
        $insert_stmt->execute();
        header('Location: ./login.php');
       }
     }
 }
 ?>
