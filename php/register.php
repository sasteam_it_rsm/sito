<?php
    require 'commons.php';
    require 'functions.php';
    require 'db_connect.php';
    sec_session_start();
 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/home.css">
    <script src="../js/jquery-3.2.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/sha512.js"></script>
    <script src="../js/forms.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
            @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>
    <title>Registrati</title>
  </head>
  <body>
    <main>
      <?php
      $depth=1;
        draw_menu($mysqli,$depth);
      ?>
      <div class="container-fluid">
        <div class="row">
          <div class="page-header col-md-12">
            <h1> <i class="fa fa-bars" aria-hidden="true"></i> Registrati</h1>
            <ol class="breadcrumb">
              <li><a href="../index.php">Home</a></li>
              <li class="active">Registrati</li>
            </ol>
          </div>
        </div>
      </div>

      <h2 class="text-center">Sei un nuovo cliente? Registrati e scopri tutti i vantaggi.</h2>
      <section class="container-fluid">
        <form class="form-horizontal" action="password_hashing.php" method="post" name="register_form">
          <fieldset>
            <div class="form-group">
              <label for="nome" class="col-lg-2 col-sm-2 control-label">Nome: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <input class="form-control" id="nome" name="nome" placeholder="Inserisci il tuo nome" type="text" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="cognome" class="col-lg-2 col-sm-2 control-label">Cognome: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                  <input class="form-control" id="cognome" name="cognome" placeholder="Inserisci il tuo cognome" type="text" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-lg-2 col-sm-2 control-label">Email: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                  <input class="form-control" id="email" name="email" placeholder="Inserisci la tua email" type="email" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="col-lg-2 col-sm-2 control-label">Password: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                  <input type="password" class="form-control" name="password" id="password" placeholder="***********">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password_confirm" class="col-lg-2 col-sm-2 control-label">Conferma password: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                  <input type="password" class="form-control" name="password_confirm" id="password_confirm" placeholder="***********">
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="telefono" class="col-lg-2 col-sm-2 control-label">Telefono: </label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                  <input class="form-control" id="telefono" name="telefono" placeholder="Inserisci il tuo numero di telefono" type="tel">
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
              <input type="button" value="REGISTRATI" class="btn btn-primary" onclick="form_register_hash(this.form, this.form.password, this.form.password_confirm);">
              </div>
            </div>
          </fieldset>
          </form>
      </section>
      <?php
      if(isset($_GET['error'])) {
         echo '<p class="text-center text-danger">Registraztione annullata: esiste già un account associato alla email proposta.</p> <br>';
      }
      ?>
  </main>
  <?php
    draw_footer();
   ?>
</body>
</html>
