<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  if(login_check($mysqli) != true) {
    die("ERR");
  }

  if(!isset($_GET["id"])) {
    die("ERR");
  }

  $id = $_GET["id"];


  if(!is_numeric($id) || $id < 0) {
    die("ERR");
  }

  if(!isset($_GET["isAdmin"])) {
    /* Prelevo se c'è la notifica più vecchia da mostrare */
    $testoNotifica = "";
    $dataNotifica = "";
    $sql = "SELECT * FROM notifica WHERE id='" . $id  . "' AND isRead=0 ORDER BY dataOra ASC LIMIT 1";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $testoNotifica = $row["testo"];
          $dataNotifica = $row["dataOra"];
        }
    }
    else die("ERR");

    /* Setto la notifica come letta */
    $stmt = $mysqli->prepare("UPDATE notifica
                              SET isRead=?
                              WHERE id='" . $id  . "'
                              AND dataOra='" . $dataNotifica . "'");
    $statoFinale = 1;
    $stmt->bind_param("i", $statoFinale);
    $stmt->execute();

    die($testoNotifica);
  }
  else {
    $sql = "SELECT * FROM notifica WHERE nuovoOrdine=1 ORDER BY dataOra ASC LIMIT 1";
    $result = $mysqli->query($sql);
    $finalTesto = "";
    $idNot = -1;
    $dataOra = "";

    $scrive = true;
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $finalTesto = $row["testo"];
          $idNot = $row["id"];
          $dataOra = $row["dataOra"];
          if($row["id"] == $_SESSION["user_id"]) {
            $scrive = false;
          }
        }

        $stmt = $mysqli->prepare("UPDATE notifica
                                  SET nuovoOrdine=?
                                  WHERE id='" . $idNot  . "'
                                  AND dataOra='" . $dataOra . "'");

        $isNuovo = 0;
        $stmt->bind_param("i", $isNuovo);
        $stmt->execute();

        if($scrive) {
          die($finalTesto);
        }
        else {
          die("ERR");
        }
    }

    die("ERR");
  }

?>
