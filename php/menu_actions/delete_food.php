<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  $status = 1;

  if(login_check($mysqli) != true) {
    die("NO_LOGIN");
  }

  if(!isset($_GET["id"])) {
    die("ERR");
  }

  $id = $_GET["id"];


  if(!is_numeric($id) || $id < 0) {
    die("ERR");
  }

  /* Controllo che il prodotto esista */
  $sql = "SELECT * FROM pietanza WHERE id='" . $id  . "'";
  $result = $mysqli->query($sql);
  if (!$result->num_rows > 0) {
    die("ERR");
  }
  if($row = $result->fetch_assoc()) {
    $fp = "../../img/upload/" . $row["percorsoImmagine"];
  }

  /* Controllo che il prodotto non sia in un ordine o in un carrello. */
  $alreadyPresent = false;

  $sql = "SELECT * FROM inserimento
            WHERE id='" . $id . "'";
  $result = $mysqli->query($sql);

  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $alreadyPresent = true;
      }
  }

  $sql = "SELECT * FROM carrello
            WHERE C_P_id='" . $id . "'";
  $result = $mysqli->query($sql);

  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $alreadyPresent = true;
      }
  }


  if($alreadyPresent) {
    /* Se il prodotto è presente da qualche parte lo rendo non disponibile */
    $stmt = $mysqli->prepare("UPDATE pietanza
                              SET disponibilita=?
                              WHERE id='" . $id . "'");
    $disponibilita = 0;
    $stmt->bind_param("i", $disponibilita);
    $stmt->execute();
    die("IN_USE");
  }
  else {
    /* Se il prodotto non è da nessuna parte lo elimino assieme ai suoi ingredienti */
    $stmt = $mysqli->prepare("DELETE FROM condimento WHERE id=?");
    $stmt->bind_param("i", $id);
    $stmt->execute();

    $stmt = $mysqli->prepare("DELETE FROM pietanza WHERE id=?");
    $stmt->bind_param("i", $id);
    $stmt->execute();

    fclose($fp);
    unlink($fp);
    die("OK");
  }
?>
