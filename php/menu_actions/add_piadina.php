<?php
  require "../commons.php";
  require "../db_connect.php";
  require "../functions.php";
  require "../FOOD_TYPE.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }

  if(login_check($mysqli) != true || $_SESSION["isAdmin"] != 1) {
    header("location: ../../index.php");
    die();
  }

  /* Controllo altri campi */
  if(!isset($_POST["nome"]) || strlen(trim($_POST["nome"])) > 50) {
      header("location: ../../menu_add_piadina.php?error=FIELDS_WRONG");
      die();
  }

  if(!isset($_POST["tempoPreparazione"])
      || !is_numeric($_POST["tempoPreparazione"])
      || $_POST["tempoPreparazione"] < 5
      || $_POST["tempoPreparazione"] > 120) {
    header("location: ../../menu_add_piadina.php?error=FIELDS_WRONG");
    die();
  }

  if(!isset($_POST["prezzo"])
      || !is_numeric($_POST["prezzo"])
      || $_POST["prezzo"] < 1
      || $_POST["prezzo"] > 50) {
    header("location: ../../menu_add_piadina.php?error=FIELDS_WRONG");
    die();
  }

  if(!isset($_POST["ingr"])) {
    header("location: ../../menu_add_piadina.php?error=INGR_WRONG");
    die();
  }

  /* Controlli immagine */
  $target_dir = "../../img/upload/";
  $uploadOk = 1;
  $imageFileType = strtolower(pathinfo(basename($_FILES["fileToUpload"]["name"]),PATHINFO_EXTENSION));
  $finalFileName = generate_random_string(45) . "." . $imageFileType;
  $target_file = $target_dir . $finalFileName;

  if(isset($_POST["submit"])) {
      $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
      if($check !== false) {
          $uploadOk = 1;
      }
      else {
        header("location: ../../menu_add_piadina.php?error=EXT_WRONG");
        die();
      }
  }

  if (file_exists($target_file)) {
      header("location: ../../menu_add_piadina.php?error=ERR");
      die();
  }

  if ($_FILES["fileToUpload"]["size"] > 2*MB) {
      header("location: ../../menu_add_piadina.php?error=TOO_BIG");
      die();
  }

  if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
      header("location: ../../menu_add_piadina.php?error=EXT_WRONG");
      die();
  }

  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
  } else {
      header("location: ../../menu_add_piadina.php?error=ERR");
      die();
  }

  $disponibilita = true;
  $tipo = FOOD_TYPE::Piadina;
  $isGF = isset($_POST["checkSenzaGlutine"]);
  $isVeg = isset($_POST["checkVegan"]);
  $isHot = isset($_POST["checkHot"]);
  date_default_timezone_set('Europe/Rome');
  $data = date("Y-m-d H:i:s");

  /* Inserimento hamburger in db */
  $stmt = $mysqli->prepare("INSERT INTO pietanza (prezzo, tempoDiCottura, nome, disponibilita, tipo, senzaGlutine, vegan, piccante, percorsoImmagine, data)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
  $stmt->bind_param("disiiiiiss",
                      $_POST["prezzo"],
                      $_POST["tempoPreparazione"],
                      $_POST["nome"],
                      $disponibilita,
                      $tipo,
                      $isGF,
                      $isVeg,
                      $isHot,
                      $finalFileName,
                      $data);
  $stmt->execute();

  /* Inserimento ingredienti in db */
  $sql = "SELECT * FROM pietanza ORDER BY id DESC LIMIT 1";
  $result = $mysqli->query($sql);

  $id = -1;
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
          $id = $row["id"];
      }
  }

  if($id == -1) {
    header("location: ../../menu_add_piadina.php?error=ERR");
    die();
  }

  $ingredienti = $_POST["ingr"];
  for($i = 0; $i < count($ingredienti); $i++) {
    $stmt = $mysqli->prepare("INSERT INTO condimento (idIngrediente, id)
                              VALUES (?, ?)");
    $stmt->bind_param("ii", $ingredienti[$i], $id);
    $stmt->execute();
  }

  $mysqli->close();

  header("location: ../../menu.php?info=new_piadina_ok");

?>
