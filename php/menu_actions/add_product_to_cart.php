<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  $status = 1;

  if(login_check($mysqli) != true) {
    die("NO_LOGIN");
  }

  if(!isset($_GET["id"]) || !isset($_GET["qty"])) {
    die("ERR");
  }

  $id = $_GET["id"];
  $qty = $_GET["qty"];

  if(!is_numeric($id) || !is_numeric($qty) || $qty < 1) {
    die("ERR");
  }

  /* Controllo che il prodotto esista */
  $sql = "SELECT * FROM pietanza WHERE id='" . $id  . "'";
  $result = $mysqli->query($sql);
  if (!$result->num_rows > 0) {
    die("ERR");
  }

  /* Controllo che il prodotto non sia già nel carrello. */
  $sql = "SELECT * FROM carrello
            WHERE id='" . $_SESSION['user_id']  . "' AND C_P_id='" . $id . "'";
  $result = $mysqli->query($sql);

  $alreadyPresent = false;
  $quantita = -1;
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $alreadyPresent = true;
        $quantita = $row["quantita"];
      }
  }


  if($alreadyPresent) {
    /* Se il prodotto è già nel carrello sommo la nuova quantità con quella preesistente */
    $stmt = $mysqli->prepare("UPDATE carrello
                              SET quantita=?
                              WHERE C_P_id='" . $id . "'
                              AND id='" . $_SESSION['user_id'] . "'");
    $finalQty = $quantita+$qty;
    $stmt->bind_param("i", $finalQty);
    $stmt->execute();
    die("OK");
  }
  else {
    /* Se il prodotto non è nel carrello lo aggiungo */
    $stmt = $mysqli->prepare("INSERT INTO carrello (C_P_id, id, quantita)
                              VALUES (?, ?, ?)");
    $stmt->bind_param("iii",
                        $id,
                        $_SESSION['user_id'],
                        $qty);
    $stmt->execute();
    die("OK");
  }
?>
