<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  $status = 1;

  if(login_check($mysqli) != true) {
    die("NO_LOGIN");
  }

  if(!isset($_GET["id"])) {
    die("ERR");
  }

  $id = $_GET["id"];


  if(!is_numeric($id) || $id < 0) {
    die("ERR");
  }

  /* Controllo che il prodotto esista */
  $sql = "SELECT * FROM pietanza WHERE id='" . $id  . "'";
  $result = $mysqli->query($sql);
  if (!$result->num_rows > 0) {
    die("ERR");
  }

  /* Riabilito il prodotto */
  $stmt = $mysqli->prepare("UPDATE pietanza
                            SET disponibilita=?
                            WHERE id='" . $id . "'");
  $disponibilita = 1;
  $stmt->bind_param("i", $disponibilita);
  $stmt->execute();
  die("OK");
?>
