<?php
define('MB', 1048576);
require "ORDER_STATUS.php";
function sec_session_start() {
        $session_name = 'sec_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedir� ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function login($email, $password, $mysqli) {
   // Usando statement sql 'prepared' non sar� possibile attuare un attacco di tipo SQL injection.
   if ($stmt = $mysqli->prepare("SELECT id, nome, cognome, email, password, telefono, salt, isAdmin FROM UTENTE WHERE email = ? LIMIT 1")) {
      $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($user_id, $nome, $cognome, $email, $db_password, $telefono, $salt, $isAdmin); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();
      $password = hash('sha512', $password.$salt); // codifica la password usando una chiave univoca.
      if($stmt->num_rows == 1) { // se l'utente esiste
         // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
         if(checkbrute($user_id, $mysqli) == true) {
            // Account disabilitato
            // Invia un e-mail all'utente avvisandolo che il suo account � stato disabilitato.
            return false;
         } else {
         if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
            // Password corretta!
               $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.

               $user_id = preg_replace("/[^0-9]+/", "", $user_id); // ci proteggiamo da un attacco XSS
               $_SESSION['user_id'] = $user_id;
               $_SESSION['nome'] = $nome;
               $_SESSION['cognome'] = $cognome;
               $_SESSION['email'] = $email;
			         $_SESSION['isAdmin'] = $isAdmin;
               $_SESSION['telefono'] = $telefono;
               $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
               // Login eseguito con successo.
               return true;
         } else {
            // Password incorretta.
            // Registriamo il tentativo fallito nel database.
            $now = time();
            $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
            return false;
         }
      }
      } else {
         // L'utente inserito non esiste.
         return false;
      }
   }
}

function checkbrute($user_id, $mysqli) {
   // Recupero il timestamp
   $now = time();
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   $valid_attempts = $now - (2 * 60 * 60);
   if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
      $stmt->bind_param('i', $user_id);
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      // Verifico l'esistenza di più di 5 tentativi di login falliti.
      if($stmt->num_rows > 5) {
         return true;
      } else {
         return false;
      }
   }
}

function getName(){
  echo $_SESSION['nome'];
}

function getTelefonno($mysqli){
  if ($stmt = $mysqli->prepare("SELECT telefono FROM UTENTE WHERE id = ? LIMIT 1")) {
     $stmt->bind_param('i', $_SESSION['user_id']); // esegue il bind del parametro '$email'.
     $stmt->execute(); // esegue la query appena creata.
     $stmt->store_result();
     $stmt->bind_result( $telefono); // recupera il risultato della query e lo memorizza nelle relative variabili.
     $stmt->fetch();
     if($stmt->num_rows == 1) {
         echo $telefono;
     }else{
       return false;
     }
   }
}

function getEmail(){
  echo $_SESSION['email'];
}

function getCognome(){
  echo $_SESSION['cognome'];
}

function getUserId(){
  echo $_SESSION['user_id'];
}


function location_resource($depth){
  if ($depth==0) {
    echo "./";
  }else {
    for ($i=0; $i < $depth ; $i++) {
      echo "../";
    }
  }

}



function login_check($mysqli) {
   // Verifica che tutte le variabili di sessione siano impostate correttamente
   if(isset($_SESSION['user_id'], $_SESSION['login_string'], $_SESSION['isAdmin'], $_SESSION['nome'], $_SESSION['telefono'])) {
     $user_id = $_SESSION['user_id'];
     $login_string = $_SESSION['login_string'];
     $isAdmin = $_SESSION['isAdmin'];
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.
     if ($stmt = $mysqli->prepare("SELECT password FROM UTENTE WHERE id = ? LIMIT 1")) {
        $stmt->bind_param('i', $user_id); // esegue il bind del parametro '$user_id'.
        $stmt->execute(); // Esegue la query creata.
        $stmt->store_result();

        if($stmt->num_rows == 1) { // se l'utente esiste
           $stmt->bind_result($password); // recupera le variabili dal risultato ottenuto.
           $stmt->fetch();
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              // Login eseguito!!!!
              return true;
           } else {
              //  Login non eseguito
              return false;
           }
        } else {
            // Login non eseguito
            return false;
        }
     } else {
        // Login non eseguito
        return false;
     }
   } else {
     // Login non eseguito
     return false;
   }
}

function echo_addresses_in_select($mysqli) {
    if ($stmt = $mysqli->prepare("SELECT indirizzoSpedizione, comune, cap, provincia FROM indirizzo i inner join utente u on i.id=u.id WHERE i.id = ?")) {
       $stmt->bind_param("i", $_SESSION['user_id']);
       $stmt->execute();
       $stmt->store_result();
       if($stmt->num_rows > 0) {
             $stmt->bind_result($indirizzoSpedizione, $comune, $cap, $provincia);
             while($stmt->fetch()) {
               echo '<option value="'.$indirizzoSpedizione.';'.$comune.';'.$provincia.'"> Indirizzo: '.$indirizzoSpedizione.' Comune: '.$comune.' CAP: '.$cap.' Provincia: '.$provincia.' </option>';
             }
        }
    }
}




// * * * * * * * * * * * * * * * * * \\
//   FUNZIONI DI UTILITA' PER TUTTI  \\
// * * * * * * * * * * * * * * * * * \\

/*
  Stampa un blocco di errore accessibile che mostra un riquadro rosso contenente il testo passato per argomento
*/
function print_error($text) {
  echo '
    <div class="alert alert-danger alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>Errore! </strong>' . $text . '
    </div>';
}

/*
  Stampa un blocco di successo accessibile che mostra un riquadro verde contenente il testo passato per argomento
*/
function print_success($text) {
  echo '
    <div class="alert alert-success alert-dismissible" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <strong>OK! </strong>' . $text . '
    </div>';
}

/*
  Crea una stringa casuale della lunghezza passata per argomento
*/
function generate_random_string($length) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}

function novita_hamburger_lg($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin){
  echo '
  <div class="thumbnail col-lg-6">
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Hamburger_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
        </div>
        </div>';

}

function novita_pizza_lg($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin){
  echo '
  <div class="thumbnail col-lg-6">
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Piadina_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
        </div>
        </div>';

}

function novita_hamburger($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin){
  echo '
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Hamburger_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
        </div>';

}

function novita_pizza($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin){
  echo '

        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Piadina_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
        </div>';

}

function novita_piadina_lg($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin) {
  echo '
    <div class="thumbnail col-lg-6">
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Piadina_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
          </div>
      </div>';
}

function novita_piadina($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin) {
  echo '
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Piadina_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>';
          echo '
      </div>';
}



function draw_hamburger($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin, $isDisp) {
  echo '<article class="col-md-5 col-sm-6 col-lg-4">

      <div class="thumbnail">
        <div class="item-img">
          <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Hamburger_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>

          <div class="container-fluid">
            <form  id="frmHam" >
              <div class="row">
                <div class="col-md-12">
                  <button type="button" onclick="addToCart(this.form);" class="btn btn-success btn-block btn-sm">
                    <i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i> <span>Acquista</span>
                  </button>
                </div>
              </div>

              <div class="row form-group margin-top-05">
                <div class="col-md-4 col-xs-4 text-right">
                  <label class="control-label" for="qta">Q.ta</label>
                </div>
                <div class="col-md-6 col-xs-6">
                  <input class="form-control" name="qty" id="qta" value=1 type="number" min="1" max="50" step="1" required>
                  <input type="text" name="idH" value="' . $id . '" readonly hidden>
                  <input type="text" name="nome" value="' . $nome . '" readonly hidden>
                </div>
              </div>
            </form>
          </div>';

          if($isAdmin) {
            echo '
          <form>
            <input type="text" name="idH" value="' . $id . '" readonly hidden>
            <button type="button" class="btn btn-danger btn-block btn-sm" onclick="deleteFood(this.form);">
              <i class="fa fa-trash fa-2x" aria-hidden="true"></i><span>Elimina</span>
            </button>
          </form>';

          if(!$isDisp) {
              echo '<br/>
            <form>
              <input type="text" name="idH" value="' . $id . '" readonly hidden>
              <button type="button" class="btn btn-warning btn-block btn-sm" onclick="enableFood(this.form);">
              <i class="fa fa-archive fa-2x" aria-hidden="true"></i><span>Rendi disponibile</span>
              </button>
            </form>';
          }
        }

        echo '
        </div>
      </div>

  </article>';




}

function draw_piadina($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin, $isDisp) {
  echo '
  <article class="col-md-5 col-sm-6 col-lg-4">

      <div class="thumbnail">
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Piadina_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>

          <div class="container-fluid">
            <form action="php/menu_actions/add_product_to_cart.php" id="frmHam" method="POST">
              <div class="row">
                <div class="col-md-12">
                  <button type="button" onclick="addToCart(this.form);" class="btn btn-success btn-block btn-sm">
                    <i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i> <span>Acquista</span>
                  </button>
                </div>
              </div>

              <div class="row form-group margin-top-05">
                <div class="col-md-4 col-xs-4 text-right">
                  <label class="control-label" for="qta">Q.ta</label>
                </div>
                <div class="col-md-6 col-xs-6">
                  <input class="form-control" name="qty" id="qta" value=1 type="number" min="1" max="50" step="1" required>
                  <input type="text" name="idH" value="' . $id . '" readonly hidden>
                  <input type="text" name="nome" value="' . $nome . '" readonly hidden>
                </div>
              </div>
            </form>
          </div>';

          if($isAdmin) {
            echo '
            <form>
              <input type="text" name="idH" value="' . $id . '" readonly hidden>
              <button type="button" class="btn btn-danger btn-block btn-sm" onclick="deleteFood(this.form);">
                <i class="fa fa-trash fa-2x" aria-hidden="true"></i><span>Elimina</span>
              </button>
            </form>';
            if(!$isDisp) {
                echo '<br/>
              <form>
                <input type="text" name="idH" value="' . $id . '" readonly hidden>
                <button type="button" class="btn btn-warning btn-block btn-sm" onclick="enableFood(this.form);">
                <i class="fa fa-archive fa-2x" aria-hidden="true"></i><span>Rendi disponibile</span>
                </button>
              </form>';
            }
          }


          echo '
        </div>
      </div>

  </article>';
}


function draw_pizza($id, $prezzo, $tempoCottura, $nome, $ingredienti, $senzaGlutine, $vegan, $piccante, $immagine, $isAdmin, $isDisp) {
  echo '
  <article class="col-md-5 col-sm-6 col-lg-4">

      <div class="thumbnail">
        <div class="item-img">
            <img class="img-fluid" src="img/upload/' . $immagine . '" alt="Pizza_' . $id . '">
        </div>

        <div class="caption container-fluid">
          <h3>' . $nome . '</h3>';

          if($senzaGlutine || $piccante || $vegan) {
            echo '<div class="row">';
            if($senzaGlutine) {
              echo '
                  <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    <img class="media-object" src="img/gluten_free.png" alt="Logo piccante" width="25px">
                  </div>';
            }

            if($piccante) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                    </div>';
            }

            if($vegan) {
              echo '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                      <img class="media-object" src="img/vegan.png" alt="Logo piccante" width="25px">
                    </div>';
            }
            echo '</div>';
          }

          echo '
          <dl class="row" style="margin-top: 10px;">
            <dt class="col-sm-4">Ingredienti</dt>
            <dd class="col-sm-8">' . rtrim(implode(', ', $ingredienti), ',') . '</dd>
            <dt class="col-sm-4">Prezzo</dt>
            <dd class="col-sm-8">' . $prezzo . '€</dd>
            <dt class="col-sm-4">Tempo</dt>
            <dd class="col-sm-8">' . $tempoCottura . ' minuti</dd>
          </dl>

          <div class="container-fluid">
            <form action="php/menu_actions/add_product_to_cart.php" id="frmHam" method="POST">
              <div class="row">
                <div class="col-md-12">
                  <button type="button" onclick="addToCart(this.form);" class="btn btn-success btn-block btn-sm">
                    <i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i> <span>Acquista</span>
                  </button>
                </div>
              </div>

              <div class="row form-group margin-top-05">
                <div class="col-md-4 col-xs-4 text-right">
                  <label class="control-label" for="qta">Q.ta</label>
                </div>
                <div class="col-md-6 col-xs-6">
                  <input class="form-control" name="qty" id="qta" value=1 type="number" min="1" max="50" step="1" required>
                  <input type="text" name="idH" value="' . $id . '" readonly hidden>
                  <input type="text" name="nome" value="' . $nome . '" readonly hidden>
                </div>
              </div>
            </form>
          </div>';

          if($isAdmin) {
            echo '
            <form>
              <input type="text" name="idH" value="' . $id . '" readonly hidden>
              <button type="button" class="btn btn-danger btn-block btn-sm" onclick="deleteFood(this.form);">
                <i class="fa fa-trash fa-2x" aria-hidden="true"></i><span>Elimina</span>
              </button>
            </form>';
            if(!$isDisp) {
                echo '<br/>
              <form>
                <input type="text" name="idH" value="' . $id . '" readonly hidden>
                <button type="button" class="btn btn-warning btn-block btn-sm" onclick="enableFood(this.form);">
                <i class="fa fa-archive fa-2x" aria-hidden="true"></i><span>Rendi disponibile</span>
                </button>
              </form>';
            }
          }


          echo '
        </div>
      </div>

  </article>';
}

function select_order_state($stato) {
    if($stato == ORDER_STATUS::Ricevuto) {
      return '
        <option value="' . ORDER_STATUS::Ricevuto . '" selected>' . ORDER_STATUS::Ricevuto . '</option>
        <option value="' . ORDER_STATUS::In_preparazione . '">' . ORDER_STATUS::In_preparazione . '</option>
        <option value="' . ORDER_STATUS::Spedito . '">' . ORDER_STATUS::Spedito . '</option>
        <option value="' . ORDER_STATUS::Consegnato . '">' . ORDER_STATUS::Consegnato . '</option>';

    }
    else if($stato == ORDER_STATUS::In_preparazione) {
      return '
        <option value="' . ORDER_STATUS::Ricevuto . '">' . ORDER_STATUS::Ricevuto . '</option>
        <option value="' . ORDER_STATUS::In_preparazione . '" selected>' . ORDER_STATUS::In_preparazione . '</option>
        <option value="' . ORDER_STATUS::Spedito . '">' . ORDER_STATUS::Spedito . '</option>
        <option value="' . ORDER_STATUS::Consegnato . '">' . ORDER_STATUS::Consegnato . '</option>';
    }
    else if($stato == ORDER_STATUS::Spedito) {
      return '
        <option value="' . ORDER_STATUS::Ricevuto . '">' . ORDER_STATUS::Ricevuto . '</option>
        <option value="' . ORDER_STATUS::In_preparazione . '">' . ORDER_STATUS::In_preparazione . '</option>
        <option value="' . ORDER_STATUS::Spedito . '" selected>' . ORDER_STATUS::Spedito . '</option>
        <option value="' . ORDER_STATUS::Consegnato . '">' . ORDER_STATUS::Consegnato . '</option>';
    }
    else {
      return '
        <option value="' . ORDER_STATUS::Ricevuto . '">' . ORDER_STATUS::Ricevuto . '</option>
        <option value="' . ORDER_STATUS::In_preparazione . '">' . ORDER_STATUS::In_preparazione . '</option>
        <option value="' . ORDER_STATUS::Spedito . '">' . ORDER_STATUS::Spedito . '</option>
        <option value="' . ORDER_STATUS::Consegnato . '" selected>' . ORDER_STATUS::Consegnato . '</option>';
    }
}





?>
