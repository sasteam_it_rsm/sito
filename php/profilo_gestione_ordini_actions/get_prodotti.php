<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  if(login_check($mysqli) != true) {
    die("ERR");
  }

  if(!isset($_GET["numOrdine"])) {
    die("ERR");
  }

  $numOrdine = $_GET["numOrdine"];
  $id = $_SESSION["user_id"];

  if(!is_numeric($numOrdine) || $numOrdine < 0) {
    die("ERR");
  }

  /* Controllo che l'ordine esista e sia della persona che l'ha chiesto */
  $sql = "SELECT * FROM ordine WHERE numOrdine='" . $numOrdine . "'";
  $result = $mysqli->query($sql);
  if (!$result->num_rows > 0) {
    die($id . " " . $numOrdine);
  }

  /* Prendo i prodotti */
  $sql = "SELECT P.nome, I.quantita FROM inserimento I
          INNER JOIN pietanza P
          ON I.id=P.id
            WHERE I.numOrdine='" . $numOrdine . "'";
  $result = $mysqli->query($sql);

  $prodotti = "";
  if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $prodotti = $prodotti . $row["quantita"] . " - " . $row["nome"] . "\n";
      }
  }

  die($prodotti);
?>
