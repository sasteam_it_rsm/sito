<?php
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  $id = $_SESSION["user_id"];
  if ($mysqli->connect_error) {
    die("ERR");
  }

  if(login_check($mysqli) != true || $_SESSION["isAdmin"] != 1) {
    die("ERR");
  }

  if(!isset($_GET["numOrdine"]) || !isset($_GET["newStato"])) {
    die("ERR");
  }

  $numOrdine = $_GET["numOrdine"];
  $stato = $_GET["newStato"];

  if(!is_numeric($numOrdine) || $numOrdine < 0) {
    die("ERR");
  }

  /* Controllo che l'ordine esista */
  $sql = "SELECT * FROM ordine WHERE numOrdine='" . $numOrdine  . "'";
  $result = $mysqli->query($sql);
  $idPersonaHaOrdine = -1;
  if (!$result->num_rows > 0) {
    die("ERR");
  }
  while($row = $result->fetch_assoc()) {
    $idPersonaHaOrdine = $row["id"];
  }


  /* Aggiorno lo stato */
  $stmt = $mysqli->prepare("UPDATE ordine
                            SET stato=?
                            WHERE numOrdine='" . $numOrdine . "'");
  $stmt->bind_param("s", $stato);
  $stmt->execute();

  /* Creo notifica */
  date_default_timezone_set('Europe/Rome');
  $data = date("Y-m-d H:i:s");
  $stmt = $mysqli->prepare("INSERT INTO notifica (id, dataOra, testo, isRead)
                            VALUES (?, ?, ?, ?)");

  if($stato == "Ricevuto") {
      $testo = "Il tuo ordine n. " . $numOrdine . " è tornato allo stato di RICEVUTO";
  }
  else if($stato == "In preparazione") {
      $testo = "Il tuo ordine n. " . $numOrdine . " è stato preso in carico ed è ora IN PREPARAZIONE!";
  }
  else if($stato == "Spedito") {
      $testo = "Il tuo ordine n. " . $numOrdine . " è stato appena SPEDITO. Lo riceverai a breve!";
  }
  else if($stato == "Consegnato") {
      $testo = "Il tuo ordine n. " . $numOrdine . " è stato CONSEGNATO all'indirizzo da te specificato!";
  }
  else {
    die("ERR");
  }

  $stato = 0;
  $stmt->bind_param("issi",
                      $idPersonaHaOrdine,
                      $data,
                      $testo,
                      $stato);
  $stmt->execute();


  $total = "";
  if ($stmt = $mysqli->prepare("SELECT numOrdine, data, stato, id, indirizzoSpedizione, comune, provincia
                                FROM ordine
                                WHERE stato!='Consegnato'
                                ORDER BY data ASC")) {
     $stmt->execute();
     $stmt->store_result();
     if($stmt->num_rows > 0) {
           $stmt->bind_result($numOrdine, $data, $stato, $id, $indirizzo, $comune, $provincia);
           while($stmt->fetch()) {
              $total = $total . '<tr>
                        <td data-label="Ordine">' . $numOrdine . '</td>
                        <td data-label="Data">' . $data . '</td>
                        <td data-label="Indirizzo">' . $indirizzo . '</td>
                        <td data-label="Comune">' . $comune . '</td>
                        <td data-label="Provincia">' . $provincia . '</td>
                        <td data-label="Prodotti">
                          <button class="btn" type="button" onclick="showProducts(' . $numOrdine . ');" class="btn btn-default btn-block btn-sm">
                            <i class="fa fa-list" aria-hidden="true"></i> <span>Visualizza prodotti</span>
                          </button>
                        </td>
                        <td data-label="Stato Ordine">
                          <form>
                            <div class="form-group">
                              <input type="text" name="numOrdine" value="'. $numOrdine . '" readonly hidden>
                              <input type="text" name="oldStato" value="'. $stato . '" readonly hidden>
                              <label for="stato" class="sr-only">Stato Ordine</label>
                              <select id="stato" name="stato" class="form-control" onchange="updateOrder(this.form);"> '.
                                select_order_state($stato)
                              .'</select>
                            </div>
                          </form>
                        </td>
                      </tr>';
           }




           die($total);
      }
      else {
        die("NO_ROWS");
      }
 }
 else {
   die("ERR");
 }
?>
