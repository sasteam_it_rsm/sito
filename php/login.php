<?php
  require "commons.php";
  require "db_connect.php";
  require "functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }
  if (login_check($mysqli) == true) {
    header('Location: ../index.php');
  }

?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/home.css">
    <script src="../js/jquery-3.2.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/sha512.js"></script>
    <script type="text/javascript" src="../js/forms.js"></script>
    <style>
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
            @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>
    <title>Login</title>
  </head>
  <body>
    <main>
      <?php
        $depth=1;
        draw_menu($mysqli,$depth);
       ?>
      <div class="container-fluid">
        <div class="row">
          <div class="page-header col-md-12">
            <h1> <i class="fa fa-bars" aria-hidden="true"></i> Login</h1>
            <ol class="breadcrumb">
              <li><a href="../index.php">Home</a></li>
              <li class="active">Carrello</li>
            </ol>
          </div>
        </div>
      </div>

      <h2 class="text-center">Accedi con i tuoi dati.</h2>
      <section class="container-fluid">
        <form class="form-horizontal" action="process_login.php" method="post" name="login_form">
          <fieldset>
            <div class="form-group">
              <label for="email" class="col-lg-2 col-sm-2 control-label">Email: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Inserisci la tua email" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="col-lg-2 col-sm-2 control-label">Password: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                  <input type="password" class="form-control" name="p" id="password" placeholder="***********">
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                <input type="button" value="ACCEDI ORA" class="btn btn-primary" onclick="formhash(this.form, this.form.password);"></input>
              </div>
            </div>
          </fieldset>
          </form>
      </section>
      <hr>
      <section class="container-fluid">
        <h2 class="text-center">Sei un nuovo cliente? Registrati qui.</h2>
        <a href="register.php" class="btn btn-warning" style="margin:auto; display:block; height:40px; width:150px">Registrati Ora</a>
      </section>

     <hr>
     <?php
     if(isset($_GET['error'])) {
        echo '<p class="text-center text-danger">Autenticazione fallita, riprova.</p> <br>';
     }
     else if(isset($_GET['error-cart'])) {
        echo '<p class="text-center text-danger">Per poter visualizzare il carrello devi effettuare il login!</p> <br>';
     }
     else if(isset($_GET['error-note'])) {
        echo '<p class="text-center text-danger">Per visualizzare le notifiche devi prima effettuare il login!</p> <br>';
     }
     ?>
  </main>
  <?php
    draw_footer();
   ?>
</body>
</html>
