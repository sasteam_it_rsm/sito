<?php
  require "../commons.php";
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  if(!isset($_GET["id"])) {
    die("ERR");
  }

  $id = $_GET["id"];

  if(!is_numeric($id)) {
    die("ERR");
  }

  /* Elimino il prodotto selezionato */
  $stmt = $mysqli->prepare("DELETE FROM carrello WHERE C_P_id=? AND id=?");
  $stmt->bind_param("ii", $id, $_SESSION['user_id']);
  $stmt->execute();
  die("OK");
?>
