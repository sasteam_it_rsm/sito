<?php
  require "../commons.php";
  require "../db_connect.php";
  require "../functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("ERR");
  }

  if(!isset($_GET["id"]) || !isset($_GET["qty"])) {
    die("ERR");
  }

  $id = $_GET["id"];
  $qty = $_GET["qty"];

  if(!is_numeric($id) || !is_numeric($qty) || $qty < 1) {
    die("ERR");
  }

  /* Aggiorno la quantità */
  $stmt = $mysqli->prepare("UPDATE carrello SET quantita=? WHERE C_P_id=? AND id=?");
  $stmt->bind_param("iii", $qty, $id, $_SESSION['user_id']);
  $stmt->execute();
  die("OK");
?>
