<?php
    require 'commons.php';
    require 'functions.php';
    require 'db_connect.php';
    sec_session_start();

    if ($mysqli->connect_error) {
      die("Connessione fallita: " . $mysqli->connect_error);
    }
    if (login_check($mysqli) != true) {
      header('Location: ../index.php');
    }
    if ($stmt = $mysqli->prepare("SELECT C_P_id, prezzo, nome, percorsoImmagine, quantita FROM carrello c inner join pietanza p on c.C_P_id=p.id WHERE c.id = ?")) {
       $stmt->bind_param("i", $_SESSION['user_id']);
       $stmt->execute();
       $stmt->store_result();
       if($stmt->num_rows < 1) {
         header("Location: ../shopping_cart.php?error=1");
       }
     }
     if ($stmt = $mysqli->prepare("SELECT indirizzoSpedizione, comune, cap, provincia FROM indirizzo i inner join utente u on i.id=u.id WHERE i.id = ?")) {
        $stmt->bind_param("i", $_SESSION['user_id']);
        $stmt->execute();
        $stmt->store_result();
        if($stmt->num_rows <  1) {
          header("Location: ../indirizzo.php?error=1");
        }
      }
 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/home.css">
    <script src="../js/jquery-3.2.1.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/sha512.js"></script>
    <script src="../js/forms.js"></script>
    <script src="../js/payment.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
      $(document).ready(function() {
        $('#cardpayment').hide();
        $('#paypalpayment').hide();
        $('#bitcoinpayment').hide();
        var radios = document.getElementsByName("radio");
        console.log(radios);
        for(var i = 0; i < radios.length; i++) {
         radios[i].onclick = function() {
           var val = this.value;
           if(val == 'am' || val == 'ma' || val == 'vi'){
             $('#cardpayment').fadeIn("slow");
             $('#cardpayment').show();
             $('#bitcoinpayment').hide();
             $('#paypalpayment').hide();
             $('#bitcoinpayment')[0].reset();
             $('#paypalpayment')[0].reset();
           }
           else if(val == 'pa'){
             $('#cardpayment').hide();
             $('#bitcoinpayment').hide();
             $('#cardpayment')[0].reset();
             $('#bitcoinpayment')[0].reset();
             $('#paypalpayment').fadeIn("slow");
             $('#paypalpayment').show();
           }
           else if(val == 'btc') {
             $('#cardpayment').hide();
             $('#paypalpayment').hide();
             $('#cardpayment')[0].reset();
             $('#paypalpayment')[0].reset();
             $('#bitcoinpayment').fadeIn("slow");
             $('#bitcoinpayment').show();
           }
         }
        }
      });
    </script>
    <style>
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
            @import url('https://fonts.googleapis.com/css?family=Raleway');
            input[type="radio"] {
              margin-top: 15px;
            }
    </style>
    <title>Checkout</title>
  </head>
  <body>
    <main>
      <?php
      $depth=1;
        draw_menu($mysqli,$depth);
      ?>
      <div class="container-fluid">
        <div class="row">
          <div class="page-header col-md-12">
            <h1> <i class="fa fa-bars" aria-hidden="true"></i> Checkout</h1>
            <ol class="breadcrumb">
              <li><a href="../index.php">Home</a></li>
              <li><a href="../shopping_cart.php">Carrello</a></li>
              <li class="active">Checkout</li>
            </ol>
          </div>
        </div>
      </div>

      <h2 class="text-center">Procedi subito al pagamento!</h2>
      <br>
      <section class="container-fluid">
        <label for="pagamento" class="col-lg-2 col-sm-2">Seleziona un metodo di pagamento: *</label>
        <div class="col-lg-6 col-sm-6">
          <div class="input-group">
              <label class="radio-inline">
                <input type="radio" id="radioam" name="radio" value="am">
                <i class="fa fa-cc-amex fa-3x" aria-hidden="true"></i>
              </label>
              <label class="radio-inline">
                <input type="radio" id="radioma" name="radio" value="ma">
                <i class="fa fa-cc-mastercard fa-3x" aria-hidden="true"></i>
              </label>
              <label class="radio-inline">
                <input type="radio" id="radiovi" name="radio" value="vi">
                <i class="fa fa-cc-visa fa-3x" aria-hidden="true"></i>
              </label>
              <label class="radio-inline">
                <input type="radio" id="radiopa" name="radio" value="pa">
                <i class="fa fa-paypal fa-3x" aria-hidden="true"></i>
              </label>
              <label class="radio-inline">
                <input type="radio" id="radiobtc" name="radio" value="btc">
                <i class="fa fa-btc fa-3x" aria-hidden="true"></i>
              </label>
          </div>
        </div>
      </section>
      <hr>
        <section class="container-fluid">
        <form class="form-horizontal" action="process_order.php" method="post" name="card_form" id="cardpayment">
          <fieldset>
            <div class="form-group">
              <label for="numero_carta" class="col-lg-2 col-sm-2 control-label">Numero carta: *</label>
              <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                  <input class="form-control" id="numero_carta" name="numero_carta" placeholder="Inserisci il numero della tua carta" type="text" required>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="intestatario_carta" class="col-lg-2 col-sm-2 control-label">Intestatario carta: *</label>
              <div class="col-lg-3 col-md-3 col-sm-3">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                  <input class="form-control" id="intestatario_carta" name="intestatario_carta" placeholder="Inserisci il tuo nome e cognome" type="text" required>
                </div>
              </div>
            </div>

          <div class="form-group">
            <label for="mese_scadenza" class="col-lg-2 col-sm-2 control-label">Scade il: *</label>
            <label for="anno_scadenza" class="col-lg-2 col-sm-2 control-label sr-only">Scade il: *</label>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                <select class="form-control" id="mese_scadenza" name="mese_scadenza" required>
                  <option selected value="0">01</option>
                  <option value="1">02</option>
                  <option value="2">03</option>
                  <option value="3">04</option>
                  <option value="4">05</option>
                  <option value="5">06</option>
                  <option value="6">07</option>
                  <option value="7">08</option>
                  <option value="8">09</option>
                  <option value="9">10</option>
                  <option value="10">11</option>
                  <option value="11">12</option>
                </select>
                <select class="form-control" id="anno_scadenza" name="anno_scadenza" required>
                  <option value="2017">2017</option>
                  <option selected value="2018">2018</option>
                  <option value="2019">2019</option>
                  <option value="2020">2020</option>
                  <option value="2021">2021</option>
                  <option value="2022">2022</option>
                  <option value="2023">2023</option>
                  <option value="2024">2024</option>
                  <option value="2025">2025</option>
                  <option value="2026">2026</option>
                  <option value="2027">2027</option>
                  <option value="2028">2028</option>
                  <option value="2029">2029</option>
                  <option value="2030">2030</option>
                  <option value="2031">2031</option>
                  <option value="2032">2032</option>
                  <option value="2033">2033</option>
                  <option value="2034">2034</option>
                  <option value="2035">2035</option>
                  <option value="2036">2036</option>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="cvv" class="col-lg-2 col-sm-2 control-label">CVV: *</label>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                <input class="form-control" id="cvv" name="cvv" placeholder="CVV" type="number" required>
              </div>
            </div>
          </div>

            <div class="form-group">
              <label for="indirizzoCompletoCarta" class="col-lg-2 col-sm-2 control-label">Indirizzo di spedizione: *</label>
              <div class="col-lg-6 col-sm-6">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-truck" aria-hidden="true"></i></div>
                  <select class="form-control" id="indirizzoCompletoCarta" name="indirizzoCompleto" required>
                    <?php
                      echo_addresses_in_select($mysqli);
                     ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="totaleCarta" class="col-lg-2 col-sm-2 control-label">Totale: </label>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="input-group">
                  <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></div>
                  <input class="form-control" id="totaleCarta" name="totale" type="text" value="<?php echo htmlspecialchars($_POST['prezzoTot']); echo "€" ?>"readonly>
                </div>
              </div>
            </div>

            <div class="form-group">
              <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
              <label for="paga_carta" class="col-lg-2 col-sm-2 sr-only control-label">Paga Ora </label>
              <input type="button" onclick="checkCard(this.form)" id=paga_carta value="PAGA ORA" class="btn btn-primary">
              </div>
            </div>
          </fieldset>
          </form>
      </section>
      <section class="container-fluid">
      <form class="form-horizontal" action="process_order.php" method="post" name="paypal_form" id="paypalpayment">
        <fieldset>
          <div class="form-group">
            <label for="indirizzoCompletoPaypal" class="col-lg-2 col-sm-2 control-label">Indirizzo di spedizione: *</label>
            <div class="col-lg-6 col-sm-6">
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-truck" aria-hidden="true"></i></div>
                <select class="form-control" id="indirizzoCompletoPaypal" name="indirizzoCompleto" required>
                  <?php
                    echo_addresses_in_select($mysqli);
                   ?>
                </select>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="totalePaypal" class="col-lg-2 col-sm-2 control-label">Totale: </label>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></div>
                <input class="form-control" id="totalePaypal" name="totale" type="text" value="<?php echo htmlspecialchars($_POST['prezzoTot']); echo "€" ?>"readonly>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
              <label for="submit" class="col-lg-2 col-sm-2 sr-only control-label">Paga con PayPal: </label>
            <input type="image" id="submit" src="../img/paypal.gif" alt="Paga Ora" name="submit">
            </div>
          </div>
        </fieldset>
        </form>
    </section>

    <section class="container-fluid">
    <form class="form-horizontal" action="process_order.php" method="post" name="bitcoin_form" id="bitcoinpayment">
      <fieldset>

        <div class="form-group">
          <label for="btc_address" class="col-lg-2 col-sm-2 control-label">Indirizzo BTC: *</label>
          <div class="col-lg-6 col-sm-6">
            <div class="input-group">
              <div class="input-group-addon"><i class="fa fa-btc" aria-hidden="true"></i></div>
              <input class="form-control" id="btc_address" name="btc_address" placeholder="Inserisci il tuo indirizzo Bitcoin" type="text" required>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="indirizzoCompletoBTC" class="col-lg-2 col-sm-2 control-label">Indirizzo di spedizione: *</label>
          <div class="col-lg-6 col-sm-6">
            <div class="input-group">
              <div class="input-group-addon"><i class="fa fa-truck" aria-hidden="true"></i></div>
              <select class="form-control" id="indirizzoCompletoBTC" name="indirizzoCompleto" required>
                <?php
                  echo_addresses_in_select($mysqli);
                 ?>
              </select>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label for="totaleBTC" class="col-lg-2 col-sm-2 control-label">Totale: </label>
          <div class="col-lg-2 col-md-2 col-sm-2">
            <div class="input-group">
              <div class="input-group-addon"><i class="fa fa-money" aria-hidden="true"></i></div>
              <input class="form-control" id="totaleBTC" name="totale"  type="text" value="<?php echo htmlspecialchars($_POST['prezzoTot']); echo "€" ?>"readonly>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
          <label for="paga_ora" class="col-lg-2 col-sm-2 sr-only control-label">Paga Ora: </label>
          <input type="button" onclick="checkBtcAddress(this.form)" id=paga_ora value="PAGA ORA" class="btn btn-primary"></input>
          </div>
        </div>
      </fieldset>
      </form>
  </section>
  </main>
  <?php
    draw_footer();
   ?>
</body>
</html>
