<?php
    require 'commons.php';
    require 'functions.php';
    require 'db_connect.php';
    sec_session_start();

    if(isset($_GET['id'])) {
      $id = $_GET['id'];
    }
    else {
      die('ERR');
    }

    if ($stmt = $mysqli->prepare("SELECT C_P_id, disponibilita from carrello c INNER JOIN pietanza p on c.C_P_id=p.id WHERE c.id=?")) {
         $stmt->bind_param('i',$id);
         // Esegui la query ottenuta.
         $stmt->execute();
         $stmt->store_result();
         $stmt->bind_result($idPietanza, $disponibilitaPietanza);
         while($stmt->fetch()) {
           if($disponibilitaPietanza == 0) {
             die('ERR');
           }
         }
         die('OK');
        }
    die('ERR');
 ?>
