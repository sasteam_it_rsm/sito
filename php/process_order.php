<?php
include 'commons.php';
include 'db_connect.php';
include 'functions.php';
sec_session_start(); // usiamo la nostra funzione per avviare una sessione php sicura
if(isset($_POST['indirizzoCompleto'], $_POST['totale'], $_POST['numero_carta'], $_POST['intestatario_carta'],
          $_POST['mese_scadenza'], $_POST['anno_scadenza'], $_POST['cvv'])) {
            $tmp = $_POST['numero_carta'];
            $numero_carta = str_replace(" ", "", $tmp);
            $intestatario_carta = $_POST['intestatario_carta'];
            $mese_scadenza = $_POST['mese_scadenza'];
            $anno_scadenza = $_POST['anno_scadenza'];
            $cvv = $_POST['cvv'];
            $mese = date("n") - 1;
            $anno = date("Y");
            echo $mese;
            echo $anno;
            if(is_numeric($numero_carta) && is_numeric($cvv) && (preg_match('/^(?:4[0-9]{12}(?:[0-9]{3})?)$/', $numero_carta) === 1
                || preg_match('/^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/', $numero_carta) === 1
                || preg_match('/^(?:3[47][0-9]{13})$/', $numero_carta) === 1) && preg_match('/^[A-Za-z ]+$/', $intestatario_carta) === 1
                && ($mese_scadenza >= $mese && $anno_scadenza >= $anno) && preg_match('/^[0-9]{3,4}$/', $cvv)) {
                  $indirizzoCompleto = $_POST['indirizzoCompleto'];
                  $totale = $_POST['totale'];
                  date_default_timezone_set('Europe/Rome');
                  $data = date("Y-m-d H:i:s");
                  $indirizzo = explode(";",$indirizzoCompleto);
                  $id = $_SESSION['user_id'];
                  $pietanze = $_SESSION['pietanze'];
                  $qta = $_SESSION['quantita'];
                  var_dump($pietanze);
                  $stato = ORDER_STATUS::Ricevuto;
                 if ($insert_stmt = $mysqli->prepare("INSERT INTO ordine (data, stato, id, indirizzoSpedizione, comune, provincia, Eff_id) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
                      $insert_stmt->bind_param('ssisssi', $data, $stato, $id, $indirizzo[0], $indirizzo[1], $indirizzo[2], $id);
                      // Esegui la query ottenuta.
                      $insert_stmt->execute();
                 }
                 if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY numOrdine DESC LIMIT 1")) {
                      $stmt->bind_param('i',$id);
                      // Esegui la query ottenuta.
                      $stmt->execute();
                      $stmt->store_result();
                      $stmt->bind_result($numOrdine);
                      $stmt->fetch();
                      echo $numOrdine;
                     }
                 for($i=0; $i < count($pietanze); $i++) {
                   if ($insert_stmt = $mysqli->prepare("INSERT INTO inserimento (id, numOrdine, quantita) VALUES (?, ?, ?)")) {
                        $insert_stmt->bind_param('iii', $pietanze[$i], $numOrdine, $qta[$i]);
                        // Esegui la query ottenuta.
                        $insert_stmt->execute();
                   }
                 }
                 if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY data DESC LIMIT 1")) {
                      $stmt->bind_param('i',$id);
                      // Esegui la query ottenuta.
                      $stmt->execute();
                      $stmt->store_result();
                      $stmt->bind_result($numOrdineData);
                      $stmt->fetch();
                     }

                  /* Creo notifica */
                  if ($stmt_notifica = $mysqli->prepare("INSERT INTO notifica (id, dataOra, testo, isRead, nuovoOrdine)
                                            VALUES (?, ?, ?, ?, ?)")) {
                  $testo = "Ordine " . $numOrdineData . " ricevuto correttamente!";
                  $stato = 0;
                  $isNuovo = 1;
                  $stmt_notifica->bind_param("issii",
                                      $id,
                                      $data,
                                      $testo,
                                      $stato,
                                      $isNuovo);
                  $stmt_notifica->execute();
                 }
                 $del_stmt = $mysqli->prepare("DELETE FROM carrello WHERE id=?");
                 if ($del_stmt) {
                      $del_stmt->bind_param('i', $id);
                      // Esegui la query ottenuta.
                      $del_stmt->execute();
                      unset($_SESSION['pietanze']);
                      unset($_SESSION['quantita']);
                      header('Location: ../ordini.php');
                     }
                }
  }
  else if (isset($_POST['indirizzoCompleto'], $_POST['totale'])) {
    $indirizzoCompleto = $_POST['indirizzoCompleto'];
    $totale = $_POST['totale'];
    date_default_timezone_set('Europe/Rome');
    $data = date("Y-m-d H:i:s");
    $indirizzo = explode(";",$indirizzoCompleto);
    $id = $_SESSION['user_id'];
    $pietanze = $_SESSION['pietanze'];
    $qta = $_SESSION['quantita'];
    var_dump($pietanze);
    $stato = ORDER_STATUS::Ricevuto;
   if ($insert_stmt = $mysqli->prepare("INSERT INTO ordine (data, stato, id, indirizzoSpedizione, comune, provincia, Eff_id) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
        $insert_stmt->bind_param('ssisssi', $data, $stato, $id, $indirizzo[0], $indirizzo[1], $indirizzo[2], $id);
        // Esegui la query ottenuta.
        $insert_stmt->execute();
   }
   if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY numOrdine DESC LIMIT 1")) {
        $stmt->bind_param('i',$id);
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($numOrdine);
        $stmt->fetch();
        echo $numOrdine;
       }
   for($i=0; $i < count($pietanze); $i++) {
     if ($insert_stmt = $mysqli->prepare("INSERT INTO inserimento (id, numOrdine, quantita) VALUES (?, ?, ?)")) {
          $insert_stmt->bind_param('iii', $pietanze[$i], $numOrdine, $qta[$i]);
          // Esegui la query ottenuta.
          $insert_stmt->execute();
     }
   }
   if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY data DESC LIMIT 1")) {
        $stmt->bind_param('i',$id);
        // Esegui la query ottenuta.
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($numOrdineData);
        $stmt->fetch();
       }
         /* Creo notifica */
    if ($stmt_notifica = $mysqli->prepare("INSERT INTO notifica (id, dataOra, testo, isRead, nuovoOrdine)
                              VALUES (?, ?, ?, ?, ?)")) {
    $testo = "Ordine " . $numOrdineData . " ricevuto correttamente!";
    $stato = 0;
    $isNuovo = 1;
    $stmt_notifica->bind_param("issii",
                        $id,
                        $data,
                        $testo,
                        $stato,
                        $isNuovo);
    $stmt_notifica->execute();
   }
   $del_stmt = $mysqli->prepare("DELETE FROM carrello WHERE id=?");
   if ($del_stmt) {
        $del_stmt->bind_param('i', $id);
        // Esegui la query ottenuta.
        $del_stmt->execute();
        unset($_SESSION['pietanze']);
        unset($_SESSION['quantita']);
        header('Location: ../ordini.php');
       }
  }
  else if (isset($_POST['indirizzoCompleto'], $_POST['totale'], $_POST['btc_address'])) {
    $btc_address = $_POST['btc_address'];
    if(preg_match('/^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$/', $btc_address)) {
      $indirizzoCompleto = $_POST['indirizzoCompleto'];
      $totale = $_POST['totale'];
      date_default_timezone_set('Europe/Rome');
      $data = date("Y-m-d H:i:s");
      $indirizzo = explode(";",$indirizzoCompleto);
      $id = $_SESSION['user_id'];
      $pietanze = $_SESSION['pietanze'];
      $qta = $_SESSION['quantita'];
      var_dump($pietanze);
      $stato = ORDER_STATUS::Ricevuto;
     if ($insert_stmt = $mysqli->prepare("INSERT INTO ordine (data, stato, id, indirizzoSpedizione, comune, provincia, Eff_id) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
          $insert_stmt->bind_param('ssisssi', $data, $stato, $id, $indirizzo[0], $indirizzo[1], $indirizzo[2], $id);
          // Esegui la query ottenuta.
          $insert_stmt->execute();
     }
     if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY numOrdine DESC LIMIT 1")) {
          $stmt->bind_param('i',$id);
          // Esegui la query ottenuta.
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($numOrdine);
          $stmt->fetch();
          echo $numOrdine;
         }
     for($i=0; $i < count($pietanze); $i++) {
       if ($insert_stmt = $mysqli->prepare("INSERT INTO inserimento (id, numOrdine, quantita) VALUES (?, ?, ?)")) {
            $insert_stmt->bind_param('iii', $pietanze[$i], $numOrdine, $qta[$i]);
            // Esegui la query ottenuta.
            $insert_stmt->execute();
       }
     }
     if ($stmt = $mysqli->prepare("SELECT numOrdine from ordine WHERE id=? ORDER BY data DESC LIMIT 1")) {
          $stmt->bind_param('i',$id);
          // Esegui la query ottenuta.
          $stmt->execute();
          $stmt->store_result();
          $stmt->bind_result($numOrdineData);
          $stmt->fetch();
         }
           /* Creo notifica */
      if ($stmt_notifica = $mysqli->prepare("INSERT INTO notifica (id, dataOra, testo, isRead, nuovoOrdine)
                                VALUES (?, ?, ?, ?, ?)")) {
      $testo = "Ordine " . $numOrdineData . " ricevuto correttamente!";
      $stato = 0;
      $isNuovo = 1;
      $stmt_notifica->bind_param("issii",
                          $id,
                          $data,
                          $testo,
                          $stato,
                          $isNuovo);
      $stmt_notifica->execute();
     }
     $del_stmt = $mysqli->prepare("DELETE FROM carrello WHERE id=?");
     if ($del_stmt) {
          $del_stmt->bind_param('i', $id);
          // Esegui la query ottenuta.
          $del_stmt->execute();
          unset($_SESSION['pietanze']);
          unset($_SESSION['quantita']);
          header('Location: ../ordini.php');
         }
    }
  }
?>
