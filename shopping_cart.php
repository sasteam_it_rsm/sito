<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }
  if (login_check($mysqli) != true) {
    header('Location: php/login.php?error-cart=1');
  }

 ?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/shopping_cart.css">
    <link rel="stylesheet" href="css/home.css">
    <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/shopping_cart.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
            @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
            @import url('https://fonts.googleapis.com/css?family=Raleway');

    </style>
    <title>Carrello</title>
  </head>
  <body>
    <main>
      <?php
        $depth=0;
        draw_menu($mysqli,$depth);
      ?>
      <div class="container-fluid">
        <div class="row">
          <div class="page-header col-md-12">
            <h1> <i class="fa fa-bars" aria-hidden="true"></i> Carrello</h1>
            <ol class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Carrello</li>
            </ol>
          </div>
        </div>
      </div>

      <div class="container">
	       <table id="cart" class="table table-hover table-condensed">
    				<thead>
						<tr>
							<th style="width:50%">Prodotto</th>
							<th style="width:10%">Prezzo</th>
							<th style="width:8%">Quantità</th>
							<th style="width:22%" class="text-center">Totale parziale</th>
						</tr>
					</thead>
              <?php
                $prezzoTot = 0;
                $pietanze = array();
                $qta = array();
                $disponibilitaProdotti = array();
                if ($stmt = $mysqli->prepare("SELECT C_P_id, prezzo, nome, percorsoImmagine, quantita, disponibilita FROM carrello c inner join pietanza p on c.C_P_id=p.id WHERE c.id = ?")) {
                   $stmt->bind_param("i", $_SESSION['user_id']);
                   $stmt->execute();
                   $stmt->store_result();
                   if($stmt->num_rows > 0) {
                         $stmt->bind_result($pietanza, $prezzo, $nome, $percorsoImmagine, $quantita, $disponibilita);
                         while($stmt->fetch()) {
                           array_push($pietanze, $pietanza);
                           array_push($qta, $quantita);
                           array_push($disponibilitaProdotti, $disponibilita);
                           $prezzoTot += $prezzo*$quantita;

                           if($disponibilita) {
                             $stato = "Disponibile";
                             $statoHtml = "text-success";
                           }
                           else {
                             $stato = "Non disponibile";
                             $statoHtml = "text-danger";
                           }
                            echo '<tbody>
                        						<tr>
                                      <td data-th="Prodotto">
                                        <div class="row">
                                          <div class="col-sm-4 hidden-xs"><img src="img/upload/'.$percorsoImmagine.'" alt="Immagine '.$nome.'" class="img-responsive"/></div>
                                          <div class="col-sm-10">
                                            <h4 class="nomargin text-muted">'.$nome.'</h4>
                                            <span class="'.$statoHtml.'"><strong>'.$stato.'</strong></span>
                                          </div>
                                        </div>
                                      </td>
                                      <td data-th="Prezzo">'.$prezzo.' €</td>
                                      <form>
                                        <td data-th="Quantità">
                                           <label for="qta" class="col-lg-2 col-sm-2 sr-only">Quantita</label>
                   								         <input type="number" class="form-control text-center" name="qta" id="qta" value="'.$quantita.'">
                       							    </td>
                                        <td data-th="Totale parziale" class="text-center">'.$prezzo*$quantita.' €</td>
                                        <td class="actions" data-th="">
                                          <input type = "hidden" name = "idPietanza" value = "'.$pietanza.'" />
                                          <label for="btn_refresh" class="col-lg-2 col-sm-2 sr-only control-label">Aggiorna</label>
                                          <button aria-label="Ricarica" type="button" class="btn btn-info btn-sm" id="btn_refresh" name="btn_refresh" onclick="refresh(this.form);" ><i class="fa fa-refresh"></i></button>
                                          <label for="btn_delete" class="col-lg-2 col-sm-2 sr-only control-label">Elimina:</label>
                                          <button aria-label="Elimina" type="button" class="btn btn-danger btn-sm" id="btn_delete" name="btn_delete" onclick="deleteFood(this.form);"><i class="fa fa-trash-o"></i></button>
                                        </td>
                                      </form>
                                    </tr>
                        					</tbody>';
                         }
                         $_SESSION['pietanze'] = $pietanze;
                         $_SESSION['quantita'] = $qta;
                    }
               }

               echo '<tfoot>
           						<tr class="visible-xs">
           							<td class="text-center"><strong>Totale '.$prezzoTot.' €</strong></td>
           						</tr>
           						<tr>
           							<td><a href="menu.php" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continua lo shopping</a></td>
           							<td colspan="2" class="hidden-xs"></td>
           							<td class="hidden-xs text-center"><strong>Totale '.$prezzoTot.' €</strong></td>
           							<td>
                          <form class="" action="php/checkout.php" method="post">
                            <input type = "hidden" name = "prezzoTot" value = "'.$prezzoTot.'" />
                            <input type="button" value="Procedi al checkout" class="btn btn-success btn-block" onclick="checkStatus(this.form, '.$_SESSION['user_id'].');">
                          </form>
                        </td>
           						</tr>
           					</tfoot>';
               ?>
				</table>
        <?php
        if(isset($_GET['error'])) {
           echo '<p class="text-center text-danger">Il tuo carrello è vuoto!.</p> <br>';
        }
        ?>
      </div>
    </main>
  <?php
    draw_footer();
   ?>
 </body>
</html>
