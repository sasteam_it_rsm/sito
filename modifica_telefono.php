<?php
require "php/commons.php";
require "php/db_connect.php";
require "php/functions.php";
  sec_session_start();

if (isset($_POST['new_phone'])) {
  $numero = $_POST['new_phone'];
  $id = $_SESSION['user_id'];
  if (preg_match('/^[0-9]{10}$/', $numero) === 1 || is_numeric($numero)) {

    if ($insert_stmt = $mysqli->prepare("UPDATE utente SET telefono = ? WHERE id = ?")) {
     $insert_stmt->bind_param('si', $numero, $id);
     // Esegui la query ottenuta.
     $insert_stmt->execute();
     header('Location: dati_personali.php');
    }


}

}
 ?>
