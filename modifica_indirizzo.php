<?php
require "php/commons.php";
require "php/db_connect.php";
require "php/functions.php";
  sec_session_start();

if (isset($_POST['indirizzo'], $_POST['comune'], $_POST['provincia'], $_POST['cap'],$_POST['indirizzo2'], $_POST['comune2'], $_POST['provincia2'], $_POST['cap2'])) {
  $indirizzo = $_POST['indirizzo'];
  $comune = $_POST['comune'];
  $provincia = $_POST['provincia'];
  $id = $_SESSION['user_id'];
  $cap = $_POST['cap'];
  $indirizzo2 = $_POST['indirizzo2'];
  $comune2 = $_POST['comune2'];
  $provincia2 = $_POST['provincia2'];
  $cap2 = $_POST['cap2'];
  if (preg_match('/^(V-|I-)?[0-9]{5}$/', $cap2) === 1 && preg_match('/^[A-Za-z ]+$/', $comune2) === 1 && preg_match('/^[A-Za-z ]+$/', $provincia2) === 1) {

    if ($insert_stmt = $mysqli->prepare("UPDATE indirizzo SET indirizzoSpedizione = ?, comune = ?, cap = ?, provincia = ? WHERE id = ? AND indirizzoSpedizione = ? AND comune = ? AND provincia = ?")) {
     $insert_stmt->bind_param('ssisisss', $indirizzo2, $comune2, $cap2, $provincia2,$id,$indirizzo,$comune,$provincia);
     // Esegui la query ottenuta.
     $insert_stmt->execute();
     header('Location: indirizzo.php');
    }
    else {
      header('Location: indirizzo.php?error=FAIL_DB');
    }
  }
  else {
    header('Location: indirizzo.php?error=REGEX');
  }

}
 ?>
