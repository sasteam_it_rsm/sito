<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
    sec_session_start();

    if(login_check($mysqli) != true) {
      header("Location: php/login.php");
    }
?>

<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/profilo.css">
    <script src="js/home.js"></script>
    <script src="js/forms.js"></script>
    <script src="js/sha512.js"></script>

    <script>
    var tmp= 0;
      $(document).ready(function(){
          $('#password_form').hide();
          $('#btn-salva-pass').hide();
          $("#btn-modifica-pass").click( function(){
            if(!tmp){
              $('#password_form').show();
              $('#btn-salva-pass').show();
              tmp=1;
            }else{
              $('#password_form').hide();
                $('#btn-salva-pass').hide();
                tmp=0;
            }
          });
      });
    </script>

    <script>
       $(document).ready(function(){
           $('[data-toggle="popover"]').popover();
       });
    </script>
    <title>Home</title>
  </head>
  <body>
    <?php
      $depth=0;
      draw_menu($mysqli,$depth);
      draw_profilo($depth);
     ?>

     <div class="container-fullwidth visible-xs-block visible-sm-block visible-md-block visible-lg-block">
       <div class="navbar navbar-inverse profilo">
         <div class="container-fluid">
           <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <h2 class="text-center">Dati personali</h2>
               <form  id="personali_form" class="form-horizontal dati" action="modifica_telefono.php" method="post" name="dati_personali_form">
                 <fieldset>
                   <div class="form-group">
                     <label for="nome" class="col-lg-2 col-sm-2 control-label">Nome: </label>
                     <div class="col-lg-6 col-sm-6">
                       <div class="input-group">
                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                         <input class="form-control" id="nome" name="nome" value="<?php getName(); ?>" type="text" required readonly>
                       </div>
                     </div>
                   </div>
                   <div class="form-group">
                     <label for="cognome" class="col-lg-2 col-sm-2 control-label">Cognome: </label>
                     <div class="col-lg-6 col-sm-6">
                       <div class="input-group">
                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                         <input class="form-control" id="cognome" name="cognome" value="<?php getCognome(); ?>" type="text" required readonly>
                       </div>
                     </div>
                   </div>

                   <div class="form-group">
                     <label for="email" class="col-lg-2 col-sm-2 control-label">Email: </label>
                     <div class="col-lg-6 col-sm-6">
                       <div class="input-group">
                         <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                         <input class="form-control" id="email" name="email" value="<?php getEmail(); ?>" type="email" required readonly>
                       </div>
                     </div>
                   </div>

                   <div class="form-group">
                     <label for="telefono" class="col-lg-2 col-sm-2 control-label">Telefono: </label>
                     <div class="col-lg-6 col-sm-6">
                       <div class="input-group">
                         <div class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                         <input class="form-control" id="telefono" name="telefono" placeholder="Ex: 0547189475" value="<?php getTelefonno($mysqli); ?>" type="tel">
                       </div>
                     </div>
                   </div>
                   <div class="form-group salva">
                     <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                       <input type="button" value="Salva" class="btn btn-default" onclick="change_tNumber(this.form);">
                        <input type="text" name="new_phone" value="" readonly hidden></input>
                     </div>
                   </div>
                 </fieldset>
                 </form>

                 <form id="password_form" class="form-horizontal dati" action="#" method="post" name="password_form" >
                   <fieldset>
                     <div class="form-group">
                       <label for="nome" class="col-lg-2 col-sm-2 control-label">Vecchia password: </label>
                       <div class="col-lg-6 col-sm-6">
                         <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                           <input class="form-control"  name="old_p1" value="" type="password" required >
                         </div>
                       </div>
                     </div>
                     <div class="form-group">
                       <label for="nome" class="col-lg-2 col-sm-2 control-label">Nuova password: </label>
                       <div class="col-lg-6 col-sm-6">
                         <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                           <input class="form-control" name="new_p" value="" type="password" required >
                         </div>
                       </div>
                     </div>

                     <div class="form-group">
                       <label for="email" class="col-lg-2 col-sm-2 control-label">Conferma password: </label>
                       <div class="col-lg-6 col-sm-6">
                         <div class="input-group">
                           <div class="input-group-addon"><i class="fa fa-at" aria-hidden="true"></i></div>
                           <input class="form-control"  name="conf_p" value="" type="password" required >
                         </div>
                       </div>
                     </div>
                   </fieldset>
               </form>

               <form class="form-horizontal dati" action="check_password_modify.php" method="post" name="dati_personali_form">
                 <div class="form-group salva">
                   <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                     <input type="button"  id="btn-salva-pass" value="salva" name="btn-salva-pass" class="btn btn-default" onclick="change_password(this.form)"></input>
                     <input type="text" name="old_p" value="" readonly hidden></input>
                     <input type="text" name="new_p" value="" readonly hidden></input>
                     <input type="text" name="conf_p" value="" readonly hidden></input>
                   </div>
                 </div>
               </form>
               <form class="form-horizontal dati" action="#" method="post" name="dati_personali_form">
                 <div class="form-group salva">
                   <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                     <input type="button"  id="btn-modifica-pass" value="Modifica password" name="btn-modifica-pass" class="btn btn-default" ></input>
                   </div>
                 </div>
               </form>

             </div>
           </div>
         </div>
       </div>
     </div>

  </body>
  <?php
      draw_footer();
    ?>
</html>
