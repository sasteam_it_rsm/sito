<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  require "php/FOOD_TYPE.php";
    sec_session_start();

    $isAdmin = false;
    if(isset($_SESSION["isAdmin"])) {
      $isAdmin = $_SESSION["isAdmin"];
    }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/default.css" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/index_ext.css">

    <link rel="stylesheet" type="text/css" href="css/noty.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-v3.css">
    <script type="text/javascript" src="js/noty.js"></script>

    <script src="js/jquery-3.2.1.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/home.js"></script>
    <script>
      function myMap() {
        var uluru = {lat: 44.139635, lng: 12.2432656};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs0ql3FM585na9w0UHgxdavSeeYNtLr0g&callback=myMap"></script>

    <script>
       $(document).ready(function(){
           $("#testimonial-slider").owlCarousel({
               items:2,
               itemsDesktop:[1000,2],
               itemsDesktopSmall:[979,2],
               itemsTablet:[768,2],
               itemsMobile:[650,1],
               pagination:true,
               navigation:false,
               slideSpeed:1000,
               autoPlay:true
           });

           $('[data-toggle="popover"]').popover();

           $('#myCarousel').carousel({
                   interval: 5000
           });

           //Handles the carousel thumbnails
           $('[id^=carousel-selector-]').click(function () {
           var id_selector = $(this).attr("id");
           try {
               var id = /-(\d+)$/.exec(id_selector)[1];
               console.log(id_selector, id);
               jQuery('#myCarousel').carousel(parseInt(id));
           } catch (e) {
               console.log('Regex failed!', e);
           }
         });
         // When the carousel slides, auto update the text
         $('#myCarousel').on('slid.bs.carousel', function (e) {
                  var id = $('.item.active').data('slide-number');
                 $('#carousel-text').html($('#slide-content-'+id).html());
         });
       });
    </script>

    <?php
    if(login_check($mysqli) == true) {
      echo "
      <script>
        $(document).ready(function() {
          function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }

          async function checkNotify(userID, isAdmin) {
            while(true) {
              if (window.XMLHttpRequest) {
                  xmlhttp = new XMLHttpRequest();
              }

              xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    var response = this.responseText;

                    if(response != 'ERR') {
                      new Noty({
                          theme: 'bootstrap-v3',
                          layout: 'bottomRight',
                          text: response,
                          type: 'info',
                          timeout: 2000
                      }).show();
                    }
                  }
              };
              xmlhttp.open('GET','php/menu_actions/get_notify.php?id=' + userID, true);
              xmlhttp.send();

              // Operazioni da fare se è un admin e deve controllare i nuovi ordini in arrivo
              if(isAdmin==1) {
                if (window.XMLHttpRequest) {
                    xmlhttp2 = new XMLHttpRequest();
                }

                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var response2 = this.responseText;

                      if(response2 != 'ERR') {
                        new Noty({
                            theme: 'bootstrap-v3',
                            layout: 'bottomRight',
                            text: response2,
                            type: 'info',
                            timeout: 3000
                        }).show();
                      }
                    }
                };
                xmlhttp2.open('GET','php/menu_actions/get_notify.php?id=' + userID + '&isAdmin=' + isAdmin, true);
                xmlhttp2.send();
              }

              await sleep(2000);
            }
          }";
          if($isAdmin) {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 1)";
          }
          else {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 0)";
          }

          echo "
        });
      </script>";
    }
    ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

    <title>Home</title>
  </head>
  <body>
    <?php
      $depth=0;
      draw_menu($mysqli,$depth);
     ?>
    
     
     <div class="page-header text-center">
       <h1>Sgulvanìd <br/><small>"Vin olta cl'è prònt ad magnè!" <br/><span style="font-size: 60%;">[nonna romagnola anonima]</span></small></h1>
     </div>
     <div class="hidden-lg hidden-md hidden-sm">

     </div>
    <div class="container hidden-xs">
      <div id="main_area">

          <div class="row">
              <div class="col-sm-6" id="slider-thumbs">

                  <ul class="hide-bullets">
                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-0">
                              <img src="img/index/1.jpg" alt="Foto Interno">
                          </a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-1"><img src="img/index/2.jpg" alt="Foto Esterno"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-2"><img src="img/index/3.jpg" alt="Piada"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-3"><img src="img/index/4.jpg" alt="Hamburger"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-4"><img src="img/index/5.jpg" alt="Piada 2"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-5"><img src="img/index/6.jpg" alt="Pizza"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-6"><img src="img/index/7.jpg" alt="Hamburger 2"></a>
                      </li>

                      <li class="col-sm-3">
                          <a class="thumbnail" id="carousel-selector-7"><img src="img/index/8.jpg" alt="Riviera"></a>
                      </li>

                  </ul>
              </div>
              <div class="col-sm-6">
                  <div class="col-xs-12" id="slider">

                      <div class="row">
                          <div class="col-sm-12" id="carousel-bounding-box">
                              <div class="carousel slide" id="myCarousel">

                                  <div class="carousel-inner">
                                      <div class="active item" data-slide-number="0">
                                          <img src="img/index/1.jpg" alt=""></div>

                                      <div class="item" data-slide-number="1">
                                          <img src="img/index/2.jpg" alt=""></div>

                                      <div class="item" data-slide-number="2">
                                          <img src="img/index/3.jpg" alt=""></div>

                                      <div class="item" data-slide-number="3">
                                          <img src="img/index/4.jpg" alt=""></div>

                                      <div class="item" data-slide-number="4">
                                          <img src="img/index/5.jpg" alt=""></div>

                                      <div class="item" data-slide-number="5">
                                          <img src="img/index/6.jpg" alt=""></div>

                                      <div class="item" data-slide-number="6">
                                          <img src="img/index/7.jpg" alt=""></div>

                                      <div class="item" data-slide-number="7">
                                          <img src="img/index/8.jpg" alt=""></div>

                                  </div>
                                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                      <span class="glyphicon glyphicon-chevron-left"></span>
                                      <span class="sr-only">Foto precedente</span>
                                  </a>
                                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                      <span class="glyphicon glyphicon-chevron-right"></span>
                                      <span class="sr-only">Foto successiva</span>
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </div>

    <hr>

     <section class="container-fluid" id="chi_siamo">
       <div class="row">
         <div class="col-lg-4 col-lg-offset-1">
           <div id="map" style="width:100%;height:450px;background:yellow"></div>
         </div>
         <div class="col-lg-6">
           <h1>
             <i class="fa fa-users" aria-hidden="true"></i>
              Chi siamo?

              Dove siamo?
              <i class="fa fa-location-arrow" aria-hidden="true"></i>
              </h1>
            <p> Sgulvanìd è un impresa italiana (per meglio dire Romagnola) che opera nel settore alimentare
             da oltre dieci anni.</p>
            <p> Da sempre rinomata per la sua qualità di prodotti nostrani e per le materie prime km 0 che riservano ai piatti spirito e anima romagnoli,
            recentemente ha deciso di modernizzarsi, per adattarsi alle esigenze dei nuovi clienti. Infatti da quest'anno offriamo anche il servizio di consegna a domicilio!</p>
            <p>
              <strong>Ma cosa vuol dire "Sgulvanìd"?</strong>
              <br/>
              La parola Sgulvanìd tradotta in un pseudo italiano significa "mangione", una persona alla quale piace abbuffarsi senza ritegno! Se questo è il tuo obiettivo, beh, sei capitato nel posto ideale!
            </p>
            <hr>
            <p>Come puoi vedere dalle foto, il nostro è un piccolo stabile nei pressi di Cesena, <strong>vieni a trovarci!</strong></p>
            <div class="row">
              <div class="col-lg-3">
                <address>
                    <strong>Indirizzo</strong><br>
                    Via Sacchi, 3<br>
                    Cesena FC<br>
                    <abbr title="Telefono">Tel:</abbr> (0547) 189475
                </address>
              </div>
              <div class="col-lg-4">
                <address>
                  <strong>Vuoi prenotare un tavolo?</strong><br>
                  <a href="mailto:info@sgulvanid.it">info@sgulvanid.it</a>
                </address>
              </div>
              <div class="col-lg-5">
                <address>
                  <strong>Intolleranze alimentari?</strong><br>
                  Sgulvanìd non lascia indietro nessuno: serviamo anche cibi <strong>senza glutine</strong> e per <strong>vegani</strong>!
                </address>
              </div>
            </div>
          </div>
       </div>
     </section>

     <hr>
     <section id="cosa_dicono">
       <div class="container-fluid">
         <div class="row">
           <div class="col">
             <h2 class="text-center"><i class="fa fa-star" aria-hidden="true"></i> Cosa dicono di noi i nostri clienti <i class="fa fa-star" aria-hidden="true"></i></h2>
           </div>

         </div>
       </div>

       <div class="container">
         <div class="row">
             <div class="col-md-12">

                 <div id="testimonial-slider" class="owl-carousel">
                     <div class="testimonial">
                         <div class="pic">
                             <img src="img/fc.gif">
                         </div>
                         <p class="description">
                             Ordinato e consumato tantissimi prodotti: spedizione veloce, prodotti di alta qualità (trovarne cosi oggi non è
                             affatto facile).

                             Personalmente voto 10/10, il cibo è ottimo, si sente proprio il sapore della Romagna, complimenti!
                         </p>
                         <div class="testimonial-profile">
                             <h3 class="title">Frank -</h3>
                             <span class="post">System Engineer</span>
                         </div>
                     </div>

                     <div class="testimonial">
                         <div class="pic">
                             <img src="img/dm.jpg">
                         </div>
                         <p class="description">
                             Uan of the best uebbsaits det ai ev ever iused!
                             <br/>
                             <br/>
                             <br/>
                             <br/>
                         </p>
                         <div class="testimonial-profile">
                             <h3 class="title">Darius -</h3>
                             <span class="post">teacher</span>
                         </div>
                     </div>

                     <div class="testimonial">
                         <div class="pic">
                             <img src="img/dam.jpg">
                         </div>
                         <p class="description">
                           Personalmente mi sono trovato bene. La spedizione è stata abbastanza veloce, il cibo era ancora caldo, nonostante i 25km!
                           La spedizione gratuita senza limiti di prezzo rende la piattaforma molto interessante, consigliato!
                         </p>
                         <div class="testimonial-profile">
                             <h3 class="title">David -</h3>
                             <span class="post">Developer</span>
                         </div>
                     </div>

                     <div class="testimonial">
                         <div class="pic">
                             <img src="img/gda.png">
                         </div>
                         <p class="description">
                           I ragazzi sono bravi, il servizio è veloce e il cibo è buono.
                           <br>
                           Peccato solo per il sito web male realizzato: niente HTTPS, ospitato su un server veramente infimo, la connessione TCP è veramente lenta, il download dei contenuti richiede troppo tempo... Assolutamente da migliorare!!!
                         </p>
                         <div class="testimonial-profile">
                             <h3 class="title">Gabriel -</h3>
                             <span class="post">Founder of <a href="https://it.wikipedia.org/wiki/Razor_1911">Razor1911</a></span>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     </section>

     <hr>

     <br/>
     <br/>

     <section id="novita" class="visible-sm-block visible-xs-block visible-md-block visible-lg-block">
         <h2 class="text-uppercase text-center"> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Novità</h2>
         <p>Ecco a te un elenco delle nostre novità. Se preferisci, a fondo pagina troverai i link al menu completo.</p>
         <p>Sai già cosa vuoi? Perfetto! Ti basta solamente <strong>digitare il nome del prodotto</strong> che preferisci direttamente <strong>nel campo Ricerca </strong>che si trova nella parte alta di ogni pagina del sito.</p>
         <div class="container-fluid">
          <div class="row">
            <?php
            $sql = "SELECT * FROM pietanza WHERE tipo='". FOOD_TYPE::Hamburger. "' ORDER BY data DESC LIMIT 1";
            $result = $mysqli->query($sql);

            if ($result->num_rows > 0) {
              if($row = $result->fetch_assoc()) {
              echo '<div class="col-sm-4 col-md-4">
                        <div class="thumbnail">
                          <img src="img/upload/'.$row['percorsoImmagine'].'" alt="Novità hamburger">
                            <div class="caption">
                            <h3>'.$row['nome'].'</h3>
                             </div>
                          </div>
                        </div>
                        ';
                      }
            }
        ?>

        <?php
          $sql = "SELECT * FROM pietanza WHERE tipo='". FOOD_TYPE::Pizza. "' ORDER BY data DESC LIMIT 1";
          $result = $mysqli->query($sql);

          if ($result->num_rows > 0) {
            if($row = $result->fetch_assoc()) {
            echo '<div class="col-sm-4 col-md-4">
                      <div class="thumbnail">
                        <img src="img/upload/'.$row['percorsoImmagine'].'" alt="Novità pizze">
                          <div class="caption">
                          <h3>'.$row['nome'].'</h3>
                           </div>
                        </div>
                      </div>
                      ';
              }
          }
        ?>

        <?php
          $sql = "SELECT * FROM pietanza WHERE tipo='". FOOD_TYPE::Piadina. "' ORDER BY data DESC LIMIT 1";
          $result = $mysqli->query($sql);

          if ($result->num_rows > 0) {
            if($row = $result->fetch_assoc()) {
            echo '<div class="col-sm-4 col-md-4">
                      <div class="thumbnail">
                        <img src="img/upload/'.$row['percorsoImmagine'].'" alt="Novità piadine">
                          <div class="caption">
                          <h3>'.$row['nome'].'</h3>
                           </div>
                        </div>
                      </div>
                      ';
              }
          }
        ?>
      </div>
    </div>
  </section>





  <hr>
  <section id="menu">
    <div class="container-fluid" style="margin-top: 20px">
      <div class="row">
        <div class="col-lg-12">
          <h2 class="text-center"> <i class="fa fa-cutlery" aria-hidden="true"></i> MENU</h2>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-4 col-md-4 text-center">
          <a href="menu.php#pizzas">
            <img src="img/index/pizza.jpg" alt="Menu Pizza" id="logoPizza" class="img-thumbnail">
          </a>
        </div>
        <div class="col-sm-4 col-md-4 text-center">
          <a href="menu.php#piadinas">
            <img src="img/index/piadina.jpg" alt="Menu Piadina" id="logoPizza" class="img-thumbnail">
          </a>
        </div>
        <div class="col-sm-4 col-md-4 text-center">
          <a href="menu.php#hamburgers">
            <img src="img/index/hamburger.jpg" alt="Menu Hamburger" id="logoPizza" class="img-thumbnail">
          </a>
        </div>
      </div>
    </div>
  </section>
  </body>
  <?php
      draw_footer();
    ?>
</html>
