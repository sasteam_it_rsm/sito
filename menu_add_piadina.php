<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }

  $isAdmin = false;
  if(isset($_SESSION["isAdmin"])) {
    $isAdmin = $_SESSION["isAdmin"];
  }

?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/menu_ext.css">
    <link rel="stylesheet" href="dist/css/bootstrap-select.css">

    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="dist/js/bootstrap-select.js"></script>

    <link rel="stylesheet" type="text/css" href="css/noty.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-v3.css">
    <script type="text/javascript" src="js/noty.js"></script>

    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <?php
    if(login_check($mysqli) == true) {
      echo "
      <script>
        $(document).ready(function() {
          function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }

          async function checkNotify(userID, isAdmin) {
            while(true) {
              if (window.XMLHttpRequest) {
                  xmlhttp = new XMLHttpRequest();
              }

              xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    var response = this.responseText;

                    if(response != 'ERR') {
                      new Noty({
                          theme: 'bootstrap-v3',
                          layout: 'bottomRight',
                          text: response,
                          type: 'info',
                          timeout: 2000
                      }).show();
                    }
                  }
              };
              xmlhttp.open('GET','php/menu_actions/get_notify.php?id=' + userID, true);
              xmlhttp.send();

              // Operazioni da fare se è un admin e deve controllare i nuovi ordini in arrivo
              if(isAdmin==1) {
                if (window.XMLHttpRequest) {
                    xmlhttp2 = new XMLHttpRequest();
                }

                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var response2 = this.responseText;

                      if(response2 != 'ERR') {
                        new Noty({
                            theme: 'bootstrap-v3',
                            layout: 'bottomRight',
                            text: response2,
                            type: 'info',
                            timeout: 3000
                        }).show();
                      }
                    }
                };
                xmlhttp2.open('GET','php/menu_actions/get_notify.php?id=' + userID + '&isAdmin=' + isAdmin, true);
                xmlhttp2.send();
              }

              await sleep(2000);
            }
          }";
          if($isAdmin) {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 1)";
          }
          else {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 0)";
          }

          echo "
        });
      </script>";
    }
    ?>
    <title>Aggiungi Piadina</title>
  </head>
  <body>
    <main id="content">
      <div class="container-fluid">
        <div class="row">
            <div class="page-header col-md-12">
              <h1> <i class="fa fa-plus-square" aria-hidden="true"></i> Aggiungi Piadina</h1>
              <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li><a href="menu.php">Menu</a></li>
                <li class="active">Aggiungi Piadina</li>
              </ol>
            </div>
        </div>
      </div>
      <?php
        if(isset($_GET["error"])) {
          $error = $_GET["error"];
          if($error == "ERR") {
            print_error("Si è verificato un errore sconosciuto nel tentativo di inserire la nuova piadina. Si prega di riprovare.");
          }
          else if($error == "TOO_BIG") {
            print_error("L'immagine scelta è troppo grande. Si prega di riprovare.");
          }
          else if($error == "EXT_WRONG") {
            print_error("L'immagine scelta è in un formato non accettato. Si prega di riprovare.");
          }
          else if($error == "FIELDS_WRONG") {
            print_error("Uno o più valori specificati per nome, tempo di cottura e prezzo non sono validi. Si prega di riprovare.");
          }
          else if($error == "INGR_WRONG") {
            print_error("La combinazione di ingredienti selezionata non è ammessa. Si prega di riprovare.");
          }
        }

        if(login_check($mysqli) == true && $_SESSION["isAdmin"] == 1) {
          echo '
            <section class="container-fluid">
              <form class="form-horizontal" action="php/menu_actions/add_piadina.php" enctype="multipart/form-data" method="POST">
                <fieldset>
                  <legend>Aggiungi nuova piadina</legend>
                  <div class="form-group">
                    <label for="inputNome" class="col-lg-2 col-sm-2 control-label">Nome</label>
                    <div class="col-lg-4 col-sm-5">
                      <input class="form-control" id="inputNome" name="nome" placeholder="Nome" type="text" maxlength="50" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputTempoPreparazione" class="col-lg-2 col-sm-2 control-label">Tempo preparazione (min)</label>
                    <div class="col-lg-2 col-sm-3">
                      <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
                        <input class="form-control" name="tempoPreparazione" id="inputTempoPreparazione" placeholder="Ex. 30" type="number" min="5" max="120" required>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="inputPrezzo" class="col-lg-2 col-sm-2 control-label">Prezzo</label>
                    <div class="col-lg-2 col-sm-3">
                      <div class="input-group">
                        <div class="input-group-addon"><i class="fa fa-eur" aria-hidden="true"></i></div>
                        <input class="form-control" name="prezzo" id="inputPrezzo" placeholder="Ex. 2.50" type="number" min="1" max="50" step="0.01" required>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="gruppoVarianti" class="col-lg-2 col-sm-2 control-label">Variante</label>
                    <div class="col-lg-2 col-sm-3" id="gruppoVarianti" name="gruppoVarianti">
                      <div class="checkbox">
                        <label><input type="checkbox" name="checkSenzaGlutine" value="GF"/>Senza glutine</label>
                      </div>
                      <div class="checkbox">
                        <label><input type="checkbox" name="checkVegan" value="V"/>Vegano</label>
                      </div>
                      <div class="checkbox">
                        <label><input type="checkbox" name="checkHot" value="H"/>Piccante</label>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                   <label for="ingredienti" class="col-lg-2 col-sm-2 control-label">Ingredienti</label>
                   <div class="col-lg-10" style="padding-left: 0px; padding-right: 0px;">
                     <select id="ingredienti" name="ingr[]" class="selectpicker col-md-8 col-xs-12 col-sm-8 col-lg-8" multiple data-live-search="true">
                        <optgroup label="Piada" data-max-options="1">';
                            $sql = "SELECT * FROM ingrediente WHERE nome LIKE 'Piada%'";
                            $result = $mysqli->query($sql);

                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value='" . $row["idIngrediente"] . "'>" . $row["nome"] . "</option>";
                                }
                            }
                        echo '
                        </optgroup>
                        <optgroup label="Condimenti">';
                            $sql = "SELECT * FROM ingrediente WHERE nome
                                    NOT LIKE 'Pane%'
                                    AND nome NOT LIKE 'Base%'
                                    AND nome NOT LIKE 'Piada%'
                                    ORDER BY nome ASC";
                            $result = $mysqli->query($sql);

                            if ($result->num_rows > 0) {
                                while($row = $result->fetch_assoc()) {
                                    echo "<option value='" . $row["idIngrediente"] . "'>" . $row["nome"] . "</option>";
                                }
                            }
                            $mysqli->close();
                        echo '
                        </optgroup>
                      </select>
                   </div>
                 </div>

                  <div class="form-group">
                    <label for="fileToUpload" class="col-lg-2 col-sm-2 col-xs-12 control-label">File input</label>
                    <div class="col-lg-5 col-sm-6 col-xs-8" name="gruppoVarianti">
                      <input type="file" name="fileToUpload" id="fileToUpload" required>
                      <p class="help-block" style="font-size: 0.9em;">Seleziona un\'immagine in formato .jpg, .png o .jpeg</p>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="col-md-4 col-lg-offset-2 col-md-offset-2 col-sm-offset-2">
                      <button type="submit" class="btn btn-primary col-md-8 col-sm-4 col-xs-6"><i class="fa fa-check fa-lg" aria-hidden="true" onclick="check();"></i>Salva</button>
                      <button type="reset" class="btn btn-default col-md-8 col-sm-4 col-xs-6"><i class="fa fa-trash fa-lg" aria-hidden="true"></i>Annulla</button>
                    </div>
                  </div>
                </fieldset>
                </form>
            </section>
          ';
        }
        else {
          print_error("Per visualizzare questa pagina devi aver fatto l'accesso come amministratore!");
        }
      ?>

    </main>

  	<?php
      draw_footer();
    ?>
  </body>
</html>
