<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  require "php/FOOD_TYPE.php";
  sec_session_start();
  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }

  $isAdmin = false;
  if(isset($_SESSION["isAdmin"])) {
    $isAdmin = $_SESSION["isAdmin"];
  }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/default.css">
		<link rel="stylesheet" type="text/css" href="css/component.css">
    <link rel="stylesheet" type="text/css" href="css/home.css">
    <link rel="stylesheet" type="text/css" href="css/noty.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-v3.css">

    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript" src="js/noty.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>
    <?php
    if(login_check($mysqli) == true) {
      echo "
      <script>
        $(document).ready(function() {
          function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
          }

          async function checkNotify(userID, isAdmin) {
            while(true) {
              if (window.XMLHttpRequest) {
                  xmlhttp = new XMLHttpRequest();
              }

              xmlhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    var response = this.responseText;

                    if(response != 'ERR') {
                      new Noty({
                          theme: 'bootstrap-v3',
                          layout: 'bottomRight',
                          text: response,
                          type: 'info',
                          timeout: 2000
                      }).show();
                    }
                  }
              };
              xmlhttp.open('GET','php/menu_actions/get_notify.php?id=' + userID, true);
              xmlhttp.send();

              // Operazioni da fare se è un admin e deve controllare i nuovi ordini in arrivo
              if(isAdmin==1) {
                if (window.XMLHttpRequest) {
                    xmlhttp2 = new XMLHttpRequest();
                }

                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var response2 = this.responseText;

                      if(response2 != 'ERR') {
                        new Noty({
                            theme: 'bootstrap-v3',
                            layout: 'bottomRight',
                            text: response2,
                            type: 'info',
                            timeout: 3000
                        }).show();
                      }
                    }
                };
                xmlhttp2.open('GET','php/menu_actions/get_notify.php?id=' + userID + '&isAdmin=' + isAdmin, true);
                xmlhttp2.send();
              }

              await sleep(2000);
            }
          }";
          if($isAdmin) {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 1)";
          }
          else {
            echo "checkNotify(" . $_SESSION["user_id"] . ", 0)";
          }

          echo "
        });
      </script>";
    }
    ?>
    <title>Menu</title>
  </head>
  <body>
    <?php draw_menu($mysqli, 0); ?>


    <main id="content">
      <div class="container-fluid">
        <div class="row">
            <div class="page-header col-md-12">
              <h1> <i class="fa fa-bars" aria-hidden="true"></i> Menu</h1>
              <ol class="breadcrumb">
                <li><a href="index.php">Home</a></li>
                <li class="active">Menu</li>
              </ol>
            </div>
        </div>
      </div>

      <?php
      if(login_check($mysqli) == true && $_SESSION["isAdmin"] == 1) {
        echo '
          <section class="container-fluid">
            <div class="row">
              <div class="col-md-2 col-sm-1"></div>
              <div class="col-md-8 col-sm-10">
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h2 class="panel-title"> <i class="fa fa-user-md" aria-hidden="true"></i> Funzioni amministratore </h2>
                  </div>
                  <div class="panel-body container-fluid">
                    <div class="row">
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-default btn-block btn-sm" onclick="document.location.href=\'menu_add_hamburger.php\';">
                          <i class="fa fa-plus-square fa-2x" aria-hidden="true"></i> <span>Aggiungi Hamburger</span>
                        </button>
                      </div>
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-default btn-block btn-sm" onclick="location.href=\'menu_add_pizza.php\';">
                          <i class="fa fa-plus-square fa-2x" aria-hidden="true"></i> <span>Aggiungi Pizza</span>
                        </button>
                      </div>
                      <div class="col-md-4">
                        <button type="submit" class="btn btn-default btn-block btn-sm" onclick="location.href=\'menu_add_piadina.php\';">
                          <i class="fa fa-plus-square fa-2x" aria-hidden="true"></i> <span>Aggiungi Piadina</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-2 col-sm-1"></div>
            </div>
          </section>';
        }
      ?>

      <section class="container-fluid">
        <div class="row">
          <div class="col-md-5 col-xs-2"></div>
          <div class="col-md-2 col-xs-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2 class="panel-title text-center">Filtro</h2>
              </div>
              <div class="panel-body fonted">
                <form action="menu.php" method="post">
                  <div class="media">
                    <div class="media-left media-middle">
                      <label class="radio-inline">
                        <input type="checkbox" id="checkVegan" name="vegan" value="vegan">
                      </label>
                    </div>
                    <div class="media-body text-center" style="padding-top: 5px">
                      Vegano
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left media-middle">
                      <label class="radio-inline">
                        <input type="checkbox" id="checkGF" name="glutenfree" value="gf">
                      </label>
                    </div>
                    <div class="media-body text-center" style="padding-top: 5px">
                      Senza Glutine
                    </div>
                  </div>
                  <div class="media">
                    <div class="media-left media-middle">
                      <label class="radio-inline">
                        <input type="checkbox" id="checkHot" name="hot" value="hot">
                      </label>
                    </div>
                    <div class="media-body text-center" style="padding-top: 5px">
                      Piccante
                    </div>
                  </div>
                  <div class="media">
                    <button type="submit" class="btn btn-primary btn-block btn-sm">
                      <i class="fa fa-filter fa-2x" aria-hidden="true"></i> <span>Filtra</span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="col-md-5 col-xs-2"></div>
        </div>
      </section>

      <?php
        if(isset($_GET["info"])) {
          echo '<section class="container-fluid">';
          echo '<div class="row">';
          echo '<div class="col-md-4"></div>';
          echo '<div class="col-md-4">';
          if($_GET["info"] == "new_ham_ok") {
            print_success("Hamburger inserito con successo!");
          } else if($_GET["info"] == "new_pizza_ok") {
            print_success("Pizza inserita con successo!");
          } else if($_GET["info"] == "new_piadina_ok") {
            print_success("Piadina inserita con successo!");
          }
          echo '</div>';
          echo '<div class="col-md-4"></div>';
          echo '</div>';
          echo "</section>";
        }
      ?>

      <section id="hamburgers">
        <div class="container-fluid">
          <h2>Hamburgers</h2>
          <hr>

            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Hamburger;
              if(isset($_POST["glutenfree"])) {
                $sql = $sql . " AND senzaGlutine=1";
              }
              if(isset($_POST["vegan"])) {
                $sql = $sql . " AND vegan=1";
              }
              if(isset($_POST["hot"])) {
                $sql = $sql . " AND piccante=1";
              }
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {

                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiHamburger = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiHamburger, $innerRow["nome"]);
                        }
                    }

                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }

                      $disp = $row["disponibilita"];
                      draw_hamburger($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiHamburger,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);
                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }
                  }
              }
            ?>
          </div>

        </div>
      </section>

      <hr>

      <section id="pizzas">
        <div class="container-fluid">
          <h2>Pizze</h2>
          <hr>

            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Pizza;
              if(isset($_POST["glutenfree"])) {
                $sql = $sql . " AND senzaGlutine=1";
              }
              if(isset($_POST["vegan"])) {
                $sql = $sql . " AND vegan=1";
              }
              if(isset($_POST["hot"])) {
                $sql = $sql . " AND piccante=1";
              }
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiPizza = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiPizza, $innerRow["nome"]);
                        }
                    }

                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }

                      $disp = $row["disponibilita"];
                      draw_pizza($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiPizza,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);
                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }
                  }
              }
            ?>
        </div>
      </section>

      <hr>

      <section id="piadinas">
        <div class="container-fluid">
          <h2>Piadine</h2>

            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Piadina;
              if(isset($_POST["glutenfree"])) {
                $sql = $sql . " AND senzaGlutine=1";
              }
              if(isset($_POST["vegan"])) {
                $sql = $sql . " AND vegan=1";
              }
              if(isset($_POST["hot"])) {
                $sql = $sql . " AND piccante=1";
              }
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiPiadina = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiPiadina, $innerRow["nome"]);
                        }
                    }

                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }

                      $disp = $row["disponibilita"];
                      draw_piadina($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiPiadina,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);

                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }
                  }
              }
            ?>


        </div>
      </section>

      <hr>

      <section class="container-fluid">
        <div class="row">
          <div class="col-md-4 col-xs-2"></div>
          <div class="col-md-4 col-xs-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2 class="panel-title">Legenda</h2>
              </div>
              <div class="panel-body fonted">
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Piccante
                  </div>
                </div>
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/gluten_free.png" alt="Logo senza glutine" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Senza Glutine
                  </div>
                </div>
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/vegan.png" alt="Logo vegan" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Vegano
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-xs-2"></div>
        </div>
      </section>
    </main>

    <?php
      draw_footer();
    ?>
  </body>
</html>
