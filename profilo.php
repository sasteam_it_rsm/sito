<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  sec_session_start();

  $isAdmin = false;
  if(isset($_SESSION["isAdmin"])) {
    $isAdmin = $_SESSION["isAdmin"];
  }
?>

<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/home.js"></script>

    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/profilo.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" type="text/css" href="css/noty.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-v3.css">
    <script type="text/javascript" src="js/noty.js"></script>


    <script>
       $(document).ready(function(){
           $('[data-toggle="popover"]').popover();
       });
     </script>

     <?php
     if(login_check($mysqli) == true) {
       echo "
       <script>
         $(document).ready(function() {
           function sleep(ms) {
             return new Promise(resolve => setTimeout(resolve, ms));
           }

           async function checkNotify(userID, isAdmin) {
             while(true) {
               if (window.XMLHttpRequest) {
                   xmlhttp = new XMLHttpRequest();
               }

               xmlhttp.onreadystatechange = function() {
                   if (this.readyState == 4 && this.status == 200) {
                     var response = this.responseText;

                     if(response != 'ERR') {
                       new Noty({
                           theme: 'bootstrap-v3',
                           layout: 'bottomRight',
                           text: response,
                           type: 'info',
                           timeout: 2000
                       }).show();
                     }
                   }
               };
               xmlhttp.open('GET','php/menu_actions/get_notify.php?id=' + userID, true);
               xmlhttp.send();

               // Operazioni da fare se è un admin e deve controllare i nuovi ordini in arrivo
               if(isAdmin==1) {
                 if (window.XMLHttpRequest) {
                     xmlhttp2 = new XMLHttpRequest();
                 }

                 xmlhttp2.onreadystatechange = function() {
                     if (this.readyState == 4 && this.status == 200) {
                       var response2 = this.responseText;

                       if(response2 != 'ERR') {
                         new Noty({
                             theme: 'bootstrap-v3',
                             layout: 'bottomRight',
                             text: response2,
                             type: 'info',
                             timeout: 3000
                         }).show();
                       }
                     }
                 };
                 xmlhttp2.open('GET','php/menu_actions/get_notify.php?id=' + userID + '&isAdmin=' + isAdmin, true);
                 xmlhttp2.send();
               }

               await sleep(2000);
             }
           }";
           if($isAdmin) {
             echo "checkNotify(" . $_SESSION["user_id"] . ", 1)";
           }
           else {
             echo "checkNotify(" . $_SESSION["user_id"] . ", 0)";
           }

           echo "
         });
       </script>";
     }
     ?>
    <title>Home</title>
  </head>
  <body>
    <?php
    $depth=0;
    draw_menu($mysqli,$depth);
      if(!login_check($mysqli) == true) {
        print_error("Per visualizzare questa pagina devi aver fatto l'accesso!");
      }
      else {
        draw_profilo($depth);
      }

     ?>
  </body>
  <?php
      draw_footer();
    ?>
</html>
