<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  sec_session_start();
?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/profilo.css">
    <link rel="stylesheet" href="css/gestione_ordini.css">
    <script type="text/javascript" src="js/home.js"></script>
    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/gestione_ordini.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <title>Profilo - Gestione Ordini (Admin)</title>
  </head>
  <body>
    <?php
      draw_menu($mysqli, 0);

      if(!login_check($mysqli) == true || !$_SESSION["isAdmin"] == 1) {
        echo '<div class="container-fluid">
          <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
            <div class="col-lg-8 col-md-8 col-sm-8">';
            print_error("Per visualizzare questa pagina devi aver fatto l'accesso come amministratore!");
            echo '</div>
            <div class="col-lg-2 col-md-2 col-sm-2"></div>
          </div>
        </div>';

      }
      else {
        draw_profilo(0);
        echo
          '<h2 class="text-center sr-only">Gestione ordini.</h2>
            <div class="container-fluid">
              <table>
               <caption>Gestione ordini</caption>
               <thead>
                 <tr>
                   <th scope="col">Ordine</th>
                   <th scope="col">Data</th>
                   <th scope="col">Indirizzo</th>
                   <th scope="col">Comune</th>
                   <th scope="col">Provincia</th>
                   <th scope="col">Prodotti</th>
                   <th scope="col">Stato Ordine</th>
                 </tr>
               </thead>
               <tbody id="corpoTabella">';
                      if ($stmt = $mysqli->prepare("SELECT numOrdine, data, stato, id, indirizzoSpedizione, comune, provincia
                                                    FROM ordine
                                                    WHERE stato!='Consegnato'")) {
                         $stmt->execute();
                         $stmt->store_result();
                         if($stmt->num_rows > 0) {
                               $stmt->bind_result($numOrdine, $data, $stato, $id, $indirizzo, $comune, $provincia);
                               while($stmt->fetch()) {
                                  echo '<tr>
                                            <td data-label="Ordine">' . $numOrdine . '</td>
                                            <td data-label="Data">' . $data . '</td>
                                            <td data-label="Indirizzo">' . $indirizzo . '</td>
                                            <td data-label="Comune">' . $comune . '</td>
                                            <td data-label="Provincia">' . $provincia . '</td>


                                            <td data-label="Prodotti">
                                              <button class="btn" type="button" onclick="showProducts(' . $numOrdine . ');" class="btn btn-default btn-block btn-sm">
                                                <i class="fa fa-list" aria-hidden="true"></i> <span>Visualizza prodotti</span>
                                              </button>
                                            </td>

                                            <td data-label="Stato Ordine">
                                              <form>
                                                <div class="form-group">
                                                  <input type="text" name="numOrdine" value="'. $numOrdine . '" readonly hidden>
                                                  <input type="text" name="oldStato" value="'. $stato . '" readonly hidden>
                                                  <label for="stato" class="sr-only">Stato Ordine</label>
                                                  <select id="stato" name="stato" class="form-control" onchange="updateOrder(this.form);"> '.
                                                    select_order_state($stato)
                                                  .'</select>
                                                </div>
                                              </form>
                                            </td>
                                          </tr>';
                               }
                          }
                          else {
                            echo "<tr><td colspan='6'>Nessun ordine disponibile</td></tr>";
                          }
                     }
            echo '
              </tbody>
            </table>
          </div>';
      }

        ?>
  </body>
  <?php
    draw_footer();
  ?>
</html>
