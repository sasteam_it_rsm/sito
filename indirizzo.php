<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
    sec_session_start();

    if(login_check($mysqli) != true) {
      header("Location: php/login.php");
    }

?>

<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <script src="js/home.js"></script>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/profilo.css">


    <script>
       $(document).ready(function(){
           $('[data-toggle="popover"]').popover();
       });
    </script>
    <title>Home</title>
  </head>
  <body>
    <?php
    $depth=0;
      draw_menu($mysqli,$depth);
      draw_profilo($depth);
     ?>
     <div class="container-fullwidth visible-xs-block visible-sm-block visible-md-block visible-lg-block">
       <div class="navbar navbar-inverse profilo">
         <div class="container-fluid">
           <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
               <h2 class="text-center">Indirizzo di spedizione</h2>
               <?php
                 $id = $_SESSION['user_id'];
                if($stmt = $mysqli->prepare("SELECT * FROM indirizzo WHERE  id = ?")){
                  $stmt->bind_param('i', $id);
                  $stmt->execute(); // esegue la query appena creata.
                  $stmt->store_result();
                  if(!$stmt->num_rows > 0) {
                    echo ' <form class="form-horizontal dati" action="indirizzo_hashing.php" method="post" name="dati_personali_form">
                           <fieldset>
                             <div class="form-group">
                               <label for="indirizzo" class="col-lg-2 col-sm-2 control-label">Indirizzo: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="indirizzo" name="indirizzo" placeholder="Inserisci il tuo indirizzo" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="comune" class="col-lg-2 col-sm-2 control-label">Comune: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="comune" name="comune" placeholder="Inserisci il tuo Comune" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="cap" class="col-lg-2 col-sm-2 control-label">Cap: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="cap" name="cap" placeholder="Inserisci il tuo Cap" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="provincia" class="col-lg-2 col-sm-2 control-label">Provincia: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="provincia" name="provincia" placeholder="Inserisci la Provincia" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group salva">
                               <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                               <input type="button" value="Salva" class="btn btn-default" onclick="form_indirizzo_hash(this.form);"></input>
                               </div>
                             </div>
                           </fieldset>
                         </form>';
                  }else{
                     $stmt->bind_result($id,$indirizzoSpedizione, $comune, $cap, $provincia);
                     $cont=0;
                    while($stmt->fetch()) {
                    echo '  <div class="container-fluid">
                            <div class="row">
                              <form id="n'.$cont.'" class="form-horizontal  dati" action="indirizzo_hashing.php" method="post" name="dati_personali_form">
                                 <fieldset>
                                   <div class="form-group">
                                     <label for="indirizzoW" class="col-lg-2 col-sm-2 control-label">Indirizzo: *</label>
                                     <div class="col-lg-6 col-sm-6">
                                       <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                         <input class="form-control" id="indirizzoW" name="indirizzo" value="'.$indirizzoSpedizione.'" type="text" required>
                                       </div>
                                     </div>
                                   </div>
                                   <div class="form-group">
                                     <label for="comuneW" class="col-lg-2 col-sm-2 control-label">Comune: *</label>
                                     <div class="col-lg-6 col-sm-6">
                                       <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                         <input class="form-control" id="comuneW" name="comune" value="'.$comune.'" type="text" required>
                                       </div>
                                     </div>
                                   </div>

                                   <div class="form-group">
                                     <label for="capW" class="col-lg-2 col-sm-2 control-label">Cap: *</label>
                                     <div class="col-lg-6 col-sm-6">
                                       <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                         <input class="form-control" id="capW" name="cap" value="'.$cap.'" type="text" required>
                                       </div>
                                     </div>
                                   </div>

                                   <div class="form-group">
                                     <label for="provW" class="col-lg-2 col-sm-2 control-label">Provincia: *</label>
                                     <div class="col-lg-6 col-sm-6">
                                       <div class="input-group">
                                         <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                         <input class="form-control" id="provW" name="provincia" value="'.$provincia.'" type="text" required>
                                       </div>
                                     </div>
                                   </div>
                                 </fieldset>
                               </form>
                            </div>
                            <div class="row">
                              <div class="button-container">
                                <form action="modifica_indirizzo.php" method="post" name="dati_personali_form">
                                   <div class="col-xs-offset-1 col-sm-offset-3 col-md-offset-4 col-lg-offset-4">
                                     <input type="button" value="Modifica" class="btn btn-default position-modifica"  onclick="form_modifica(this.form);"></input>
                                     <input type="text" name="id" value="'.$id.'" readonly hidden></input>
                                     <input type="text" name="indirizzo" value="'.$indirizzoSpedizione.'" readonly hidden></input>
                                     <input type="text" name="cap" value="'.$cap.'" readonly hidden></input>
                                     <input type="text" name="comune" value="'.$comune.'" readonly hidden></input>
                                     <input type="text" name="provincia" value="'.$provincia.'" readonly hidden></input>
                                     <input type="text" name="number" value="'.$cont++ .'" readonly hidden></input>
                                     <input type="text" name="indirizzo2" value="" readonly hidden></input>
                                     <input type="text" name="cap2" value="" readonly hidden></input>
                                     <input type="text" name="comune2" value="" readonly hidden></input>
                                     <input type="text" name="provincia2" value="" readonly hidden></input>
                                   </div>
                                 </form>
                                 <form action="elimina_indirizzo.php" method="post" name="dati_personali_form">
                                    <div class="">
                                      <input type="button" value="Elimina" class="btn btn-danger  position-elimina" onclick="form_delete(this.form);"></input>
                                      <input type="text" name="id" value="'.$id.'" readonly hidden></input>
                                      <input type="text" name="indirizzo" value="'.$indirizzoSpedizione.'" readonly hidden></input>
                                      <input type="text" name="comune" value="'.$comune.'" readonly hidden></input>
                                      <input type="text" name="provincia" value="'.$provincia.'" readonly hidden></input>
                                      <input type="text" name="cap" value="'.$cap.'" readonly hidden></input>
                                    </div>
                                </form>
                              </div>
                            </div>
                          </div>';
                    }
                    echo ' <form id="aggiunta_indirizzo" class="form-horizontal dati" action="indirizzo_hashing.php" method="post" name="dati_personali_form">
                           <fieldset>
                             <div class="form-group">
                               <label for="nomeT" class="col-lg-2 col-sm-2 control-label">Indirizzo: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="nomeT" name="indirizzo" placeholder="Inserisci il tuo indirizzo" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="comuneT" class="col-lg-2 col-sm-2 control-label">Comune: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="comuneT" name="comune" placeholder="Inserisci il tuo Comune" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="capT" class="col-lg-2 col-sm-2 control-label">Cap: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="capT" name="cap" placeholder="Inserisci il tuo Cap" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <label for="provinciaT" class="col-lg-2 col-sm-2 control-label">Provincia: *</label>
                               <div class="col-lg-6 col-sm-6">
                                 <div class="input-group">
                                   <div class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                   <input class="form-control" id="provinciaT" name="provincia" placeholder="Inserisci la Provincia" type="text" required>
                                 </div>
                               </div>
                             </div>

                             <div class="form-group">
                               <div class="col-lg-2 col-sm-2 col-lg-offset-2 col-sm-offset-2">
                               <input type="button" value="Aggiungi" class="btn btn-success" onclick="form_indirizzo_hash(this.form);"></input>
                               </div>
                             </div>
                           </fieldset>
                         </form>';
                  }
                }
                ?>
             </div>
           </div>
         </div>
       </div>
     </div>
     <?php
     if(isset($_GET['error'])) {
        echo '<p class="text-center text-danger">Per procedere al pagamento devi prima inserire un indirizzo di spedizione!</p> <br>';
     }
    ?>
  </body>
  <?php
      draw_footer();
    ?>
</html>
