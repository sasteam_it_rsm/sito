-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 10.0.1              
-- * Generator date: Jan 10 2017              
-- * Generation date: Tue Nov 28 12:09:45 2017 
-- * LUN file: C:\Users\Pop\Desktop\SITObackup.lun 
-- * Schema: DB/1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database DB;
use DB;


-- Tables Section
-- _____________ 

create table carrello (
     C_P_id int not null,
     id int not null,
     quantità int not null,
     constraint IDcarrello primary key (C_P_id, id));

create table INDIRIZZO (
     id int not null,
     indirizzoSpedizione varchar(50) not null,
     comune varchar(20) not null,
     cap int not null,
     provincia varchar(20) not null,
     constraint IDINDIRIZZO primary key (id, indirizzoSpedizione, comune, provincia));

create table INGREDIENTE (
     idIngrediente int not null,
     nome varchar(30) not null,
     constraint IDINGREDIENTE_ID primary key (idIngrediente));

create table condimento (
     idIngrediente int not null,
     id int not null,
     constraint IDcondimento primary key (idIngrediente, id));

create table inserimento (
     id int not null,
     numOrdine int not null,
     constraint IDinserimento primary key (id, numOrdine));

create table NOTIFICA (
     id int not null,
     dataOra date not null,
     testo varchar(100) not null,
     isRead char not null,
     constraint IDNOTIFICA primary key (id, dataOra));

create table ORDINE (
     numOrdine int not null,
     data varchar(20) not null,
     stato varchar(20) not null,
     id int not null,
     indirizzoSpedizione varchar(50) not null,
     comune varchar(20) not null,
     provincia varchar(20) not null,
     Eff_id int not null,
     constraint IDORDINE_ID primary key (numOrdine));

create table PIETANZA (
     id int not null,
     prezzo -- Compound attribute -- not null,
     tempoDiCottura char(1) not null,
     nome varchar(20) not null,
     disponibilita char not null,
     tipo varchar(3) not null,
     constraint IDPIETANZA_ID primary key (id));

create table UTENTE (
     id int not null,
     nome varchar(30) not null,
     cognome varchar(30) not null,
     email varchar(50) not null,
     password varchar(20) not null,
     isAdmin char not null,
     constraint IDUTENTE_ID primary key (id));


-- Constraints Section
-- ___________________ 

alter table carrello add constraint FKcar_UTE
     foreign key (id)
     references UTENTE (id);

alter table carrello add constraint FKcar_PIE
     foreign key (C_P_id)
     references PIETANZA (id);

alter table INDIRIZZO add constraint FKrisiede
     foreign key (id)
     references UTENTE (id);

-- Not implemented
-- alter table INGREDIENTE add constraint IDINGREDIENTE_CHK
--     check(exists(select * from condimento
--                  where condimento.idIngrediente = idIngrediente)); 

alter table condimento add constraint FKcon_PIE
     foreign key (id)
     references PIETANZA (id);

alter table condimento add constraint FKcon_ING
     foreign key (idIngrediente)
     references INGREDIENTE (idIngrediente);

alter table inserimento add constraint FKins_ORD
     foreign key (numOrdine)
     references ORDINE (numOrdine);

alter table inserimento add constraint FKins_PIE
     foreign key (id)
     references PIETANZA (id);

alter table NOTIFICA add constraint FKricezione
     foreign key (id)
     references UTENTE (id);

-- Not implemented
-- alter table ORDINE add constraint IDORDINE_CHK
--     check(exists(select * from inserimento
--                  where inserimento.numOrdine = numOrdine)); 

alter table ORDINE add constraint FKspedizione
     foreign key (id, indirizzoSpedizione, comune, provincia)
     references INDIRIZZO (id, indirizzoSpedizione, comune, provincia);

alter table ORDINE add constraint FKeffettuazione
     foreign key (Eff_id)
     references UTENTE (id);

-- Not implemented
-- alter table PIETANZA add constraint IDPIETANZA_CHK
--     check(exists(select * from inserimento
--                  where inserimento.id = id)); 

-- Not implemented
-- alter table PIETANZA add constraint IDPIETANZA_CHK
--     check(exists(select * from condimento
--                  where condimento.id = id)); 

-- Not implemented
-- alter table PIETANZA add constraint IDPIETANZA_CHK
--     check(exists(select * from carrello
--                  where carrello.C_P_id = id)); 

-- Not implemented
-- alter table UTENTE add constraint IDUTENTE_CHK
--     check(exists(select * from INDIRIZZO
--                  where INDIRIZZO.id = id)); 


-- Index Section
-- _____________ 

