-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 28, 2017 alle 11:24
-- Versione del server: 5.5.57-0+deb8u1
-- PHP Version: 5.6.30-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `WEB17_18`
--
CREATE DATABASE IF NOT EXISTS `WEB17_18` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `WEB17_18`;

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE IF NOT EXISTS `carrello` (
  `C_P_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `quantità` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `condimento`
--

CREATE TABLE IF NOT EXISTS `condimento` (
  `idIngrediente` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `INDIRIZZO`
--

CREATE TABLE IF NOT EXISTS `INDIRIZZO` (
  `id` int(11) NOT NULL,
  `indirizzoSpedizione` varchar(50) NOT NULL,
  `comune` varchar(20) NOT NULL,
  `cap` int(11) NOT NULL,
  `provincia` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `INGREDIENTE`
--

CREATE TABLE IF NOT EXISTS `INGREDIENTE` (
`idIngrediente` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `inserimento`
--

CREATE TABLE IF NOT EXISTS `inserimento` (
  `id` int(11) NOT NULL,
  `numOrdine` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `NOTIFICA`
--

CREATE TABLE IF NOT EXISTS `NOTIFICA` (
  `id` int(11) NOT NULL,
  `dataOra` datetime NOT NULL,
  `testo` varchar(100) NOT NULL,
  `isRead` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `ORDINE`
--

CREATE TABLE IF NOT EXISTS `ORDINE` (
`numOrdine` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `stato` varchar(20) NOT NULL,
  `id` int(11) NOT NULL,
  `indirizzoSpedizione` varchar(50) NOT NULL,
  `comune` varchar(20) NOT NULL,
  `provincia` varchar(20) NOT NULL,
  `Eff_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `PIETANZA`
--

CREATE TABLE IF NOT EXISTS `PIETANZA` (
`id` int(11) NOT NULL,
  `prezzo` float(4,2) NOT NULL,
  `tempoDiCottura` char(1) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `disponibilita` char(1) NOT NULL,
  `tipo` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `UTENTE`
--

CREATE TABLE IF NOT EXISTS `UTENTE` (
`id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `cognome` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `isAdmin` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrello`
--
ALTER TABLE `carrello`
 ADD PRIMARY KEY (`C_P_id`,`id`), ADD KEY `FKcar_UTE` (`id`);

--
-- Indexes for table `condimento`
--
ALTER TABLE `condimento`
 ADD PRIMARY KEY (`idIngrediente`,`id`), ADD KEY `FKcon_PIE` (`id`);

--
-- Indexes for table `INDIRIZZO`
--
ALTER TABLE `INDIRIZZO`
 ADD PRIMARY KEY (`id`,`indirizzoSpedizione`,`comune`,`provincia`);

--
-- Indexes for table `INGREDIENTE`
--
ALTER TABLE `INGREDIENTE`
 ADD PRIMARY KEY (`idIngrediente`);

--
-- Indexes for table `inserimento`
--
ALTER TABLE `inserimento`
 ADD PRIMARY KEY (`id`,`numOrdine`), ADD KEY `FKins_ORD` (`numOrdine`);

--
-- Indexes for table `NOTIFICA`
--
ALTER TABLE `NOTIFICA`
 ADD PRIMARY KEY (`id`,`dataOra`);

--
-- Indexes for table `ORDINE`
--
ALTER TABLE `ORDINE`
 ADD PRIMARY KEY (`numOrdine`), ADD KEY `FKspedizione` (`id`,`indirizzoSpedizione`,`comune`,`provincia`), ADD KEY `FKeffettuazione` (`Eff_id`);

--
-- Indexes for table `PIETANZA`
--
ALTER TABLE `PIETANZA`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `UTENTE`
--
ALTER TABLE `UTENTE`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `INGREDIENTE`
--
ALTER TABLE `INGREDIENTE`
MODIFY `idIngrediente` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ORDINE`
--
ALTER TABLE `ORDINE`
MODIFY `numOrdine` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `PIETANZA`
--
ALTER TABLE `PIETANZA`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `UTENTE`
--
ALTER TABLE `UTENTE`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
ADD CONSTRAINT `FKcar_PIE` FOREIGN KEY (`C_P_id`) REFERENCES `PIETANZA` (`id`),
ADD CONSTRAINT `FKcar_UTE` FOREIGN KEY (`id`) REFERENCES `UTENTE` (`id`);

--
-- Limiti per la tabella `condimento`
--
ALTER TABLE `condimento`
ADD CONSTRAINT `FKcon_ING` FOREIGN KEY (`idIngrediente`) REFERENCES `INGREDIENTE` (`idIngrediente`),
ADD CONSTRAINT `FKcon_PIE` FOREIGN KEY (`id`) REFERENCES `PIETANZA` (`id`);

--
-- Limiti per la tabella `INDIRIZZO`
--
ALTER TABLE `INDIRIZZO`
ADD CONSTRAINT `FKrisiede` FOREIGN KEY (`id`) REFERENCES `UTENTE` (`id`);

--
-- Limiti per la tabella `inserimento`
--
ALTER TABLE `inserimento`
ADD CONSTRAINT `FKins_PIE` FOREIGN KEY (`id`) REFERENCES `PIETANZA` (`id`),
ADD CONSTRAINT `FKins_ORD` FOREIGN KEY (`numOrdine`) REFERENCES `ORDINE` (`numOrdine`);

--
-- Limiti per la tabella `NOTIFICA`
--
ALTER TABLE `NOTIFICA`
ADD CONSTRAINT `FKricezione` FOREIGN KEY (`id`) REFERENCES `UTENTE` (`id`);

--
-- Limiti per la tabella `ORDINE`
--
ALTER TABLE `ORDINE`
ADD CONSTRAINT `FKeffettuazione` FOREIGN KEY (`Eff_id`) REFERENCES `UTENTE` (`id`),
ADD CONSTRAINT `FKspedizione` FOREIGN KEY (`id`, `indirizzoSpedizione`, `comune`, `provincia`) REFERENCES `INDIRIZZO` (`id`, `indirizzoSpedizione`, `comune`, `provincia`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
