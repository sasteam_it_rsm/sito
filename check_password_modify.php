
<script src="js/sha512.js"></script>

<?php

require "php/commons.php";
require "php/db_connect.php";
require "php/functions.php";
sec_session_start();

if ($mysqli->connect_error) {
  die("Connessione fallita: " . $mysqli->connect_error);
}

$vecchia=  $_POST['old_p'];
$new_p = $_POST['new_p'];
$conf_p = $_POST['conf_p'];
// Usando statement sql 'prepared' non sar� possibile attuare un attacco di tipo SQL injection.
if ($stmt = $mysqli->prepare("SELECT id, password, salt FROM UTENTE WHERE id=?")) {
   $stmt->bind_param('i', $_SESSION['user_id']); // esegue il bind del parametro '$email'.
   $stmt->execute(); // esegue la query appena creata.
   $stmt->store_result();
   $stmt->bind_result($user_id, $db_password, $salt); // recupera il risultato della query e lo memorizza nelle relative variabili.
   $stmt->fetch();
   $vecchia = hash('sha512',$vecchia.$salt);
   if($vecchia==$db_password){
     if($new_p==$conf_p){
       $new_p=hash('sha512',$new_p.$salt);
       if ($insert_stmt = $mysqli->prepare("UPDATE utente SET password = ? WHERE id = ?")) {
        $insert_stmt->bind_param('si', $new_p, $_SESSION['user_id']);
        // Esegui la query ottenuta.
        $insert_stmt->execute();
        header('Location: php/logout.php');
       }
     }
   }else{
       header('Location: dati_personali.php');
   }
}


 ?>
