<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  require "php/FOOD_TYPE.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }

  $isAdmin = false;
  $ricerca = "-1";
  if(isset($_POST["ricerca"])) {
    $ricerca = $_POST["ricerca"];
  }
?>

<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/footer.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link rel="stylesheet" type="text/css" href="css/default.css" />
		<link rel="stylesheet" type="text/css" href="css/component.css" />
    <link rel="stylesheet" type="text/css" href="css/home.css" />

    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <style>
      @import url(https://fonts.googleapis.com/css?family=Alegreya+Sans:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic);
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>

    <title>Ricerca</title>
  </head>
  <body>

    <?php draw_menu($mysqli, 0); ?>


    <main id="content">
      <div class="container-fluid">
        <div class="row">
            <div class="page-header col-md-12">
              <h1> <i class="fa fa-bars" aria-hidden="true"></i> Ricerca</h1>
              <ol class="breadcrumb">
                <li><a href="../index.php">Home</a></li>
                <li class="active">Ricerca</li>
              </ol>
            </div>
        </div>
      </div>

      <section>
        <div class="container-fluid">
          <h2>Hamburgers</h2>

          <div class="row">
            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Hamburger . " AND nome LIKE '%" . $ricerca . "%'";
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiHamburger = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiHamburger, $innerRow["nome"]);
                        }
                    }



                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }
                      $disp = $row["disponibilita"];
                      draw_hamburger($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiHamburger,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);
                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }

                  }
              }
            ?>

          </div>
        </div>
      </section>

      <hr>

      <section>
        <div class="container-fluid">
          <h2>Pizze</h2>

          <div class="row">
            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Pizza . " AND nome LIKE '%" . $ricerca . "%'";
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiPizza = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiPizza, $innerRow["nome"]);
                        }
                    }

                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }
                      $disp = $row["disponibilita"];
                      draw_pizza($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiPizza,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);

                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }
                  }
              }
            ?>

          </div>
        </div>
      </section>

      <hr>

      <section>
        <div class="container-fluid">
          <h2>Piadine</h2>

          <div class="row">
            <?php
              $sql = "SELECT * FROM pietanza WHERE tipo=" . FOOD_TYPE::Piadina . " AND nome LIKE '%" . $ricerca . "%'";
              $result = $mysqli->query($sql);
              $i = 0;
              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) {
                    $sql = "SELECT * FROM condimento C
                              INNER JOIN ingrediente I
                              ON C.idIngrediente=I.idIngrediente
                              WHERE C.id='" . $row["id"]  . "'";
                    $innerResult = $mysqli->query($sql);

                    $ingredientiPiadina = array();
                    if ($innerResult->num_rows > 0) {
                        while($innerRow = $innerResult->fetch_assoc()) {
                          array_push($ingredientiPiadina, $innerRow["nome"]);
                        }
                    }

                    if($row["disponibilita"] == 1 || $isAdmin) {
                      if($i == 0) {
                        echo '<div class="row">';
                        echo '<div class="visible-lg-block col-lg-2"></div>';
                        echo '<div class="visible-md-block col-md-1"></div>';
                      }
                      $disp = $row["disponibilita"];
                      draw_piadina($row["id"],
                          $row["prezzo"],
                          $row["tempoDiCottura"],
                          $row["nome"], $ingredientiPiadina,
                          $row["senzaGlutine"],
                          $row["vegan"],
                          $row["piccante"],
                          $row["percorsoImmagine"],
                          $isAdmin,
                          $disp);

                      $i++;
                      if($i == 2) {
                        echo '<div class="visible-md-block col-md-1"></div>';
                        echo '<div class="visible-lg-block col-md-2"></div>';
                        echo "</div>";
                        $i = 0;
                      }
                    }
                  }
              }
            ?>

          </div>
        </div>
      </section>

      <hr>

      <section class="container-fluid">
        <div class="row">
          <div class="col-md-4 col-xs-2"></div>
          <div class="col-md-4 col-xs-8">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h2 class="panel-title">Legenda</h2>
              </div>
              <div class="panel-body fonted">
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/hot.png" alt="Logo piccante" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Piccante
                  </div>
                </div>
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/gluten_free.png" alt="Logo senza glutine" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Senza Glutine
                  </div>
                </div>
                <div class="media">
                  <div class="media-left media-middle">
                      <img class="media-object" src="img/vegan.png" alt="Logo vegan" width="25px">
                  </div>
                  <div class="media-body text-center">
                    Vegano
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-xs-2"></div>
        </div>
      </section>
    </main>

    <?php
      draw_footer();
    ?>
  </body>
</html>
