<?php
require "php/commons.php";
require "php/db_connect.php";
require "php/functions.php";
  sec_session_start();
/*
function delete_tmp($id,$indirizzo,$comune,$cap,$provincia){
  if ($insert_stmt = $mysqli->prepare("DELETE FROM indirizzo WHERE (id=$id, indirizzoSpedizione=$indirizzo, comune=$comune, cap=$cap, provincia=$provincia)")) {


   $insert_stmt->bind_param('issis', $id, $indirizzo, $comune, $cap, $provincia);
   // Esegui la query ottenuta.
   $insert_stmt->execute();
   header('Location: indirizzo.php');
  }
}
*/

  if (isset($_POST['indirizzo'], $_POST['comune'], $_POST['cap'], $_POST['provincia'])) {

      $indirizzo = $_POST['indirizzo'];
      $comune = $_POST['comune'];
      $cap = $_POST['cap'];
      $provincia = $_POST['provincia'];
      $id = $_SESSION['user_id'];
      if (preg_match('/^(V-|I-)?[0-9]{5}$/', $cap) === 1 && preg_match('/^[A-Za-z ]+$/', $comune) === 1 && preg_match('/^[A-Za-z ]+$/', $provincia) === 1) {
        if ($insert_stmt = $mysqli->prepare("INSERT INTO indirizzo (id, indirizzoSpedizione, comune, cap, provincia) VALUES (?, ?, ?, ?, ?)")) {

         $insert_stmt->bind_param('issis', $id, $indirizzo, $comune, $cap, $provincia);
         // Esegui la query ottenuta.
         $insert_stmt->execute();
         header('Location: indirizzo.php');
        }
      }


}
 ?>
