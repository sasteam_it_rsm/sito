<?php
  require "php/commons.php";
  require "php/db_connect.php";
  require "php/functions.php";
  sec_session_start();

  if ($mysqli->connect_error) {
    die("Connessione fallita: " . $mysqli->connect_error);
  }

  if (login_check($mysqli) != true) {
    header('Location: php/login.php?error-note=1');
  }
?>
<!DOCTYPE html>
<html lang="it-IT">
  <head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <style>
      @import url(//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css);
      @import url('https://fonts.googleapis.com/css?family=Raleway');
    </style>


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/profilo.css">
    <link rel="stylesheet" href="css/gestione_ordini.css">
    <script type="text/javascript" src="js/home.js"></script>
    <script type="text/javascript" src="js/jquery-3.2.1.js"></script>
    <script type="text/javascript" src="js/gestione_ordini.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	
    <title>Notifiche</title>
  </head>
  <body>
    <?php
      draw_menu($mysqli, 0);
        echo
          '<h2 class="text-center sr-only">Gestione ordini.</h2>
            <div class="container-fluid">
              <table>
               <caption>Centro notifiche</caption>
               <thead>
                 <tr>
                   <th scope="col">Data</th>
                   <th scope="col">Notifica</th>
                 </tr>
               </thead>
               <tbody id="corpoTabella">';
                      if ($stmt = $mysqli->prepare("SELECT dataOra, testo
                                                    FROM notifica
                                                    WHERE id=? ORDER BY dataOra ASC")) {
                         $stmt->bind_param("i", $_SESSION['user_id']);
                         $stmt->execute();
                         $stmt->store_result();
                         if($stmt->num_rows > 0) {
                               $stmt->bind_result($data, $testo);
                               while($stmt->fetch()) {
                                  echo '<tr>
                                            <td data-label="Data">' . $data . '</td>
                                            <td data-label="Testo">' . $testo . '</td>
                                          </tr>';
                               }
                          }
                          else {
                            echo "<tr><td colspan='6'>Nessuna notifica disponibile!</td></tr>";
                          }
                     }
            echo '
              </tbody>
            </table>
          </div>';
        ?>
  </body>
  <?php
    draw_footer();
  ?>
</html>
