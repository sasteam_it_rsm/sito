<?php
require "php/commons.php";
require "php/db_connect.php";
require "php/functions.php";
  sec_session_start();

if (isset($_POST['indirizzo'], $_POST['comune'], $_POST['provincia'], $_POST['cap'])) {
  $indirizzo = $_POST['indirizzo'];
  $comune = $_POST['comune'];
  $provincia = $_POST['provincia'];
  $cap = $_POST['cap'];
  $id = $_SESSION['user_id'];
  $ordini = [];
  $check = 0;



  if ($stmtOrd = $mysqli->prepare("SELECT numOrdine, stato from ordine WHERE indirizzoSpedizione=? AND comune=? AND provincia=? ORDER BY numOrdine ASC")) {
       $stmtOrd->bind_param('sss',$indirizzo, $comune, $provincia);
       // Esegui la query ottenuta.
       $stmtOrd->execute();
       $stmtOrd->store_result();
       $stmtOrd->bind_result($numOrdine, $stato);
       while ($stmtOrd->fetch()) {
         if($stato != ORDER_STATUS::Consegnato) {
           $check = 1;
           $stmtOrd->close();
           header('Location: ordini.php?error=1');
         }
         array_push($ordini, $numOrdine);
       }
       // tutti gli ordini sono consegnati. posso eliminare l'indirizzo
       // elimino tabella inserimento
       if (!$check) {
       $del_stmt_ins = $mysqli->prepare("DELETE FROM inserimento WHERE numOrdine=?");
       if ($del_stmt_ins) {
         for($i = 0; $i < count($ordini); $i++) {
            $del_stmt_ins->bind_param('i', $ordini[$i]);
            // Esegui la query ottenuta.
            $del_stmt_ins->execute();
          }
        }


      //elimino tabella ordini
      $del_stmt_ord = $mysqli->prepare("DELETE FROM ordine WHERE numOrdine=?");
      if ($del_stmt_ord) {
        for($i = 0; $i < count($ordini); $i++) {
           $del_stmt_ord->bind_param('i', $ordini[$i]);
           // Esegui la query ottenuta.
           $del_stmt_ord->execute();
         }
      }
      // elimino l'indirizzo
      $stmt = $mysqli->prepare("DELETE FROM indirizzo WHERE id = ? AND indirizzoSpedizione = ? AND comune = ?  AND provincia= ?");
      $stmt->bind_param('isss', $id, $indirizzo, $comune,  $provincia);
      // Esegui la query ottenuta.
      $stmt->execute();
      $stmt->close();
      header('Location: indirizzo.php');
    }
  }
}
 ?>
