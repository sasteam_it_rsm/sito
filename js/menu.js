function addToCart(form) {
  var qty = form.qty.value;
  var id = form.idH.value;
  var nome = form.nome.value;


  if (isNaN(qty) || isNaN(id) || qty < 1) {
      swal({
        title: "Impossibile aggiungere il prodotto al carrello!",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "OK") {
              swal({
                title: "OK",
                text: qty + " " + nome + " aggiunto/i correttamente al carrello!",
                icon: "success",
                timer: 3000
              });
            } else if("NO_LOGIN"){
              swal({
                title: "Errore",
                text: "Devi aver fatto il login per poter inserire prodotti nel carrello!",
                icon: "error",
                timer: 3000
              });
            } else {
              swal({
                title: "Errore",
                text: "Impossibile aggiungere il prodotto al carrello!",
                icon: "error",
                timer: 3000
              });
            }
          }
      };
      xmlhttp.open("GET","php/menu_actions/add_product_to_cart.php?id=" + id + "&qty=" + qty, true);
      xmlhttp.send();
  }
}

function deleteFood(form) {
  var id = form.idH.value;

  if (isNaN(id) || id < 0) {
      swal({
        title: "Impossibile eliminare il prodotto!",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "OK") {
              location.reload();
            } else if("IN_USE"){
              location.reload();
            } else {
              swal({
                title: "Errore",
                text: "Impossibile eliminare il prodotto!",
                icon: "error",
                timer: 3000
              });
            }
          }
      };
      xmlhttp.open("GET","php/menu_actions/delete_food.php?id=" + id, true);
      xmlhttp.send();
  }
}

function enableFood(form) {
  var id = form.idH.value;

  if (isNaN(id) || id < 0) {
      swal({
        title: "Impossibile abilitare il prodotto!",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "OK") {
              location.reload();
            } else {
              swal({
                title: "Errore",
                text: "Impossibile abilitare il prodotto!",
                icon: "error",
                timer: 3000
              });
            }
          }
      };
      xmlhttp.open("GET","php/menu_actions/enable_food.php?id=" + id, true);
      xmlhttp.send();
  }
}
