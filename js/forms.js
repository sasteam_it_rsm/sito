function formhash(form, password) {
   // Crea un elemento di input che verr� usato come campo di output per la password criptata.
   var p = document.createElement("input");
   // Aggiungi un nuovo elemento al tuo form.
   form.appendChild(p);
   p.name = "p";
   p.type = "hidden"
   p.value = hex_sha512(password.value);
   // Assicurati che la password non venga inviata in chiaro.
   password.value = "";
   // Come ultimo passaggio, esegui il 'submit' del form.
   form.submit();
}

function form_register_hash(form, password, password_confirm) {
  // Crea un elemento di input che verr� usato come campo di output per la password criptata.
  var regexNC = /^[A-Za-z ]+$/;
  var regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var regexTel = /^\d{10}$/;
  if(password.value != password_confirm.value) {
    swal({
        title: "Le password non corrispondono!",
        icon: "error",
        timer: 3000
      });
  }
  else if(password.value.length < 6) {
    swal({
        title: "Inserisci una password con almeno 6 caratteri!",
        icon: "error",
        timer: 3000
      });
  }
  else if(!form.nome.value.match(regexNC) || !form.cognome.value.match(regexNC)) {
    swal({
        title: "Non sono ammessi numeri nel nome o nel cognome!",
        icon: "error",
        timer: 3000
      });
  }
  else if(!regexMail.test(form.email.value)) {
    swal({
        title: "L'indirizzo email inserito non è valido!",
        icon: "error",
        timer: 3000
      });
  }
  else if(form.telefono.value.length > 0 && !regexTel.test(form.telefono.value)) {
    swal({
        title: "Il numero di telefono inserito non è valido!",
        icon: "error",
        timer: 3000
      });
  }

  else {
    var p = document.createElement("input");
    // Aggiungi un nuovo elemento al tuo form.
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden"
    p.value = hex_sha512(password.value);
    // Assicurati che la password non venga inviata in chiaro.
    password.value = "";
    // Come ultimo passaggio, esegui il 'submit' del form.
    form.submit();
  }
}

function change_tNumber(form){
  var x = form.telefono.value;
  var regexN = /^[0-9]{10}$/;

  if(!x.match(regexN)){
    swal({
        title: "Il numero di telefono inserito non è valido!",
        icon: "error",
        timer: 3000
      });
  }else {
    form["new_phone"].value=x;
    form.submit();
  }

}

function change_password(form){
    var old_p = document.forms["password_form"]['old_p1'].value;
    var conf_p = document.forms["password_form"]['conf_p'].value;
    var new_p = document.forms["password_form"]['new_p'].value;
    form["old_p"].value= old_p;
    form['conf_p'].value = conf_p;
    form['new_p'].value = new_p;

    if(new_p==""){
      swal({
          title: "Inserisci una password con almeno 6 caratteri!",
          icon: "error",
          timer: 3000
        });
    }else if(new_p != conf_p){
      swal({
          title: "Le password non corrispondono!",
          icon: "error",
          timer: 3000
        });
    }else{
          var x = document.createElement("input");
          var y = document.createElement("input");
          var z = document.createElement("input");
          form.appendChild(x);
          form.appendChild(y);
          form.appendChild(z);
          x.name = "old_p";
          x.type = "hidden";
          x.value = hex_sha512(old_p);
          y.name = "conf_p";
          y.type = "hidden";
          y.value = hex_sha512(conf_p);
          z.name = "new_p";
          z.type = "hidden";
          z.value = hex_sha512(new_p);
          old_p = "";
          new_p = "";
          conf_p = "";
          form.submit();
    }


}
