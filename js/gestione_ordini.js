function updateOrder(form) {
  var newStato = form.stato.value;
  var oldStato = form.oldStato.value;
  var numOrdine = form.numOrdine.value;

  if (isNaN(numOrdine) || numOrdine < 0) {
      swal({
        title: "Impossibile modificare lo stato dell'ordine!",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "ERR") {
              swal({
                title: "Errore",
                text: "Impossibile modificare lo stato dell'ordine!",
                icon: "error",
                timer: 3000
              });
            }
            else {
              swal({
                title: "OK",
                text: "Stato dell'ordine " + numOrdine + " modificato con successo \n (da \"" + oldStato + "\" a \"" + newStato + "\")",
                icon: "success",
                timer: 3000
              });
              if(response != "NO_ROWS") {
                document.getElementById("corpoTabella").innerHTML = response;
              }
              else {
                document.getElementById("corpoTabella").innerHTML = "<tr><td colspan='6'>Nessun ordine disponibile</td></tr>";
              }
            }
          }
      };
      xmlhttp.open("GET","php/profilo_gestione_ordini_actions/cambia_stato.php?numOrdine=" + numOrdine + "&newStato=" + newStato, true);
      xmlhttp.send();
  }
}

function showProducts(ordine) {
  if (isNaN(ordine) || ordine < 0) {
      swal({
        title: "Impossibile visualizzare i prodotti!",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }

      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "ERR") {
              swal({
                title: "Errore",
                text: "Impossibile visualizzare i prodotti!",
                icon: "error",
                timer: 3000
              });
            }
            else {
              swal({
                title: "Dettagli",
                text: response,
                
              });
            }
          }
      };
      xmlhttp.open("GET","php/profilo_gestione_ordini_actions/get_prodotti.php?numOrdine=" + ordine, true);
      xmlhttp.send();
  }
}
