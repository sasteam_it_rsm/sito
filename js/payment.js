function checkCard(form) {
  var tmp = form.numero_carta.value;
  var numero_carta = tmp.replace(/\s/g, "");
  var intestatario_carta = form.intestatario_carta.value;
  var mese_scadenza = form.mese_scadenza.value;
  var anno_scadenza = form.anno_scadenza.value;
  var cvv = form.cvv.value;
  var date = new Date();
  var mese = date.getMonth();
  var anno = date.getFullYear();

  var visaRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/;
  var mastercardRegEx = /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/;
  var amexpRegEx = /^(?:3[47][0-9]{13})$/;
  var regexNC = /^[A-Za-z ]+$/;
  var regexCVV = /^[0-9]{3,4}$/;

  if(!visaRegEx.test(numero_carta) && !mastercardRegEx.test(numero_carta) && !amexpRegEx.test(numero_carta)) {
    swal({
        title: "La carta inserita non è valida!",
        icon: "error",
        timer: 3000
      });
      return;
  }
  else if(!intestatario_carta.match(regexNC)) {
    swal({
        title: "Intestatario non corretto!",
        icon: "error",
        timer: 3000
      });
      return;
  }
  else if(mese_scadenza < mese || anno_scadenza < anno) {
    swal({
        title: "Scadenza carta non corretta!",
        icon: "error",
        timer: 3000
      });
      return;
  }
  else if (!cvv.match(regexCVV)) {
    swal({
        title: "Il CVV inserito non è valido!",
        icon: "error",
        timer: 3000
      });
      return;
  }
  else {
    form.submit();
  }

}
function checkBtcAddress(form) {
  var btc = form.btc_address.value;
  var btcRegex = /^[13][a-km-zA-HJ-NP-Z0-9]{26,33}$/;

  if(!btc.match(btcRegex)) {
    swal({
        title: "L'indirizzo inserito non è valido!",
        icon: "error",
        timer: 3000
      });
      return;
  }
  else {
    form.submit();
  }
}
