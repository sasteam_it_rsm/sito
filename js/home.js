
function form_indirizzo_hash(form){
  var indirizzo = form["indirizzo"].value.trim();
  var cap = form["cap"].value.trim();
  var comune = form["comune"].value.trim();
  var provincia = form["provincia"].value.trim();

  var regexCAP = /^(V-|I-)?[0-9]{5}$/;
  var regexComune = /^[A-Za-z ]+$/;
  var regexProvicia = /^[A-Za-z ]+$/;

  if (!cap.match(regexCAP)
      || !comune.match(regexComune)
      || !provincia.match(regexProvicia)
      || indirizzo == ""
      || cap == ""
      || comune == ""
      || provincia == "") {
    swal({
        title: "inserire dei valori validi",
        icon: "error",
        timer: 3000
      });
      return;
  } else {
      form.submit();
  }
}

function form_delete(form){
    form.submit();
}

function form_modifica(form){
  var n = form['number'].value;
  var indirizzo =document.forms["n"+n]['indirizzo'].value;
  var cap = document.forms["n"+n]['cap'].value;
  var comune = document.forms["n"+n]['comune'].value;
  var provincia = document.forms["n"+n]['provincia'].value;

  var regexCAP = /^(V-|I-)?[0-9]{5}$/;
  var regexComune = /^[A-Za-z ]+$/;
  var regexProvicia = /^[A-Za-z ]+$/;


    if (!cap.match(regexCAP) || !comune.match(regexComune) || !provincia.match(regexProvicia)) {
      swal({
          title: "inserire dei valori validi",
          icon: "error",
          timer: 3000
        });
        return;
    }else {
        form['cap2'].value = document.forms["n"+n]['cap'].value;
        form['indirizzo2'].value = document.forms["n"+n]['indirizzo'].value;
        form['comune2'].value = document.forms["n"+n]['comune'].value;
        form['provincia2'].value = document.forms["n"+n]['provincia'].value;
        form.submit();
    }


}

function error_pass(){
  swal({
      title: "La vecchia password non e’ corretta!",
      icon: "error",
      timer: 3000
    });
}

function submitForm(action)
{
    document.getElementById('columnarForm').action = action;
    document.getElementById('columnarForm').submit();
}
