function refresh(form) {
  var qty = form.qta.value;
  var id = form.idPietanza.value;

  if (isNaN(qty) || isNaN(id) || qty < 1) {
      swal({
        title: "Errore: la quantità scelta non può essere negativa",
        icon: "error",
        timer: 3000
      });

  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;

            if(response == "OK") {
              location.reload();
            } else {
              swal({
                title: "Errore",
                text: "Impossibile aggiornare la quantità del prodotto." + response,
                icon: "error",

              });
            }
          }
      };
      xmlhttp.open("GET","php/shopping_cart_actions/action.php?id=" + id + "&qty=" + qty, true);
      xmlhttp.send();
  }
}

function deleteFood(form) {
  var id = form.idPietanza.value;

  if (isNaN(id)) {
      swal({
        title: "Errore: impossibile eliminare il prodotto.",
        icon: "error",
        timer: 3000
      });

  } else {
      if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
      }
      xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            if(response == "OK") {
              location.reload();
            } else {
              swal({
                title: "Errore",
                text: "Impossibile impossibile eliminare il prodotto." + response,
                icon: "error",
              });
            }
          }
      };
      xmlhttp.open("GET","php/shopping_cart_actions/delete.php?id=" + id , true);
      xmlhttp.send();
  }
}

function checkStatus(form, id) {
  if (window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
  }
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var response = this.responseText;
        if(response == "OK") {
          form.submit();
        } else {
          swal({
            title: "Errore",
            text: "Uno o più prodotti non sono disponibili!",
            icon: "error",
          });
        }
      }
  };
  xmlhttp.open("GET","php/checkCart.php?id=" + id , true);
  xmlhttp.send();
}
